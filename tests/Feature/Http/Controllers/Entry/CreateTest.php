<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\StringyForm\Http\Controllers\Entry\EntryController;
use Smorken\StringyForm\Http\Controllers\Entry\SaveController;
use Smorken\StringyForm\Models\Eloquent\EntryData;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\CanStripNonChars;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\HasForms;
use Tests\Smorken\StringyForm\TestBenchTestCase;

class CreateTest extends TestBenchTestCase
{
    use CanStripNonChars, DatabaseMigrations, HasForms;

    public function testCreateSimpleFormPostCanCreateEntry(): void
    {
        $user = User::factory()->create();
        $form = $this->getSimpleForm();
        $response = $this->actingAs($user)
            ->post('/c/'.$form->id, ['test' => 'hello world', 'check-test' => '1']);
        $response->assertRedirect('/');
        $this->assertDatabaseHas(EntryData::class, ['element' => 'test', 'value' => '"hello world"']);
        $this->assertDatabaseHas(EntryData::class, ['element' => 'check-test', 'value' => '"1"']);
    }

    public function testCreateSimpleFormPostFailsValidation(): void
    {
        $user = User::factory()->create();
        $form = $this->getSimpleForm();
        $response = $this->actingAs($user)
            ->post('/c/'.$form->id, []);
        $expected = [
            'test' => [
                'The test field is required.',
            ],
        ];
        $this->assertEquals($expected, $response->baseResponse->exception->errors());
    }

    public function testCreateSimpleFormView(): void
    {
        $user = User::factory()->create();
        $form = $this->getSimpleForm();
        $response = $this->actingAs($user)
            ->get('/c/'.$form->id);
        $expected = '<html> <head> </head> <body> <div class="content"> <div class="stringy-form"> <h4>Entry [] for Form [1]</h4> <form method="post"> <label for="test">Test</label> <input type="text" value="" name="test" id="test"/> <label for="check-test-1"> <input type="hidden" name="check-test" id="check-test-0" value="0"/> <input type="checkbox" name="check-test" id="check-test-1" value="1" /> </label> <button type="submit">Submit</button> </form> </div> </div> </body> </html>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    public function testCreateComplexFormView(): void
    {
        $user = User::factory()->create();
        $form = $this->getComplexForm();
        $response = $this->actingAs($user)
            ->get('/c/'.$form->id);
        $expected = '<html> <head> </head> <body> <div class="content"> <div class="stringy-form"> <h4>Entry [] for Form [1]</h4> <form method="post"> <div class="form-group"> <label for="name" class="form-label" > Name </label> <input id="name" type="text" class="form-control" name="name" value maxlength="255" /> </div> <div class="form-group"> <label for="location" class="form-label" > Location </label> <select id="location" class="form-select" name="location" > <option value="" >Select one</option> <option value="a" >Location A</option> <option value="b" >Location B</option> </select> </div> <div class="form-check"> <input type="hidden" name="active" value="0"> <input id="active-1" type="checkbox" class="form-check-input" name="active" value="1" /> <label for="active-1" class="form-check-label" > Active </label> </div> <fieldset> <legend>Favorite Colors</legend> <div class="form-check"> <input id="fave-colors-blue" type="checkbox" class="form-check-input" name="fave_colors[]" value="blue" /> <label for="fave-colors-blue" class="form-check-label" > Blue </label> </div> <div class="form-check"> <input id="fave-colors-green" type="checkbox" class="form-check-input" name="fave_colors[]" value="green" /> <label for="fave-colors-green" class="form-check-label" > Green </label> </div> <div class="form-check"> <input id="fave-colors-red" type="checkbox" class="form-check-input" name="fave_colors[]" value="red" /> <label for="fave-colors-red" class="form-check-label" > Red </label> </div> </fieldset> <fieldset> <legend>Best University</legend> <div class="form-check"> <input id="uni-asu" type="radio" class="form-check-input" name="uni" value="asu" /> <label for="uni-asu" class="form-check-label" > ASU </label> </div> <div class="form-check"> <input id="uni-arizona" type="radio" class="form-check-input" name="uni" value="arizona" /> <label for="uni-arizona" class="form-check-label" > UofA </label> </div> </fieldset> <div> <button id="submit-form" type="submit" name="submit_form" class="btn" > Save </button> <a id="back-link" href="http://localhost" title="Back" class="" > Back </a> </div> </form> </div> </div> </body> </html>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    protected function defineWebRoutes($router)
    {
        $router->get('/', [EntryController::class, 'index']);
        $router->get('/c/{formId}', [EntryController::class, 'createEntry']);
        $router->post('/c/{formId}', [SaveController::class, 'doCreateEntry']);
    }
}
