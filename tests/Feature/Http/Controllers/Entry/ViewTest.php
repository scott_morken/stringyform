<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\StringyForm\Http\Controllers\Entry\EntryController;
use Smorken\StringyForm\Models\Eloquent\Entry;
use Smorken\StringyForm\Models\Eloquent\EntryData;
use Smorken\StringyForm\Models\Eloquent\StringyForm;
use Tests\Smorken\StringyForm\TestBenchTestCase;

class ViewTest extends TestBenchTestCase
{
    use DatabaseMigrations;

    public function testViewEntryByFormIdHasViewOnlyModelNoElements(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $response = $this->actingAs($user)
            ->get('/vf/'.$form->id);
        $response->assertSee("Entry [{$entry->id}] for Form [{$form->id}]");
        $response->assertSee('<div>Test: </div>', false);
    }

    public function testViewEntryByFormIdHasViewOnlyModelWithElement(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'foo'])->create();
        $response = $this->actingAs($user)
            ->get('/vf/'.$form->id);
        $response->assertSee("Entry [{$entry->id}] for Form [{$form->id}]");
        $response->assertSee('<div>Test: foo</div>', false);
    }

    public function testViewEntryByFormIdHasViewOnlyNoEntryModel(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $response = $this->actingAs($user)
            ->get('/vf/'.$form->id);
        $response->assertSee("Entry [1] for Form [{$form->id}]");
        $response->assertSee('<div>Test: </div>', false);
    }

    public function testViewEntryByFormIdNoFormModelFound(): void
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->get('/vf/1');
        $response->assertSee('Not Found');
        $response->assertStatus(404);
        $this->assertInstanceOf(ModelNotFoundException::class, $response->baseResponse->exception);
    }

    public function testViewEntryByFormIdWithoutViewOnlyModelNoElements(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create(['view_only' => null]);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $response = $this->actingAs($user)
            ->get('/vf/'.$form->id);
        $response->assertSee("Entry [{$entry->id}] for Form [{$form->id}]");
        $response->assertSee('fw-bold">test:</div>', false);
        $response->assertSee('<div class="col-md-10 col-sm-12"></div>', false);
    }

    public function testViewEntryByFormIdWithoutViewOnlyModelWithElements(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create(['view_only' => null]);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'foo'])->create();
        $response = $this->actingAs($user)
            ->get('/vf/'.$form->id);
        $response->assertSee("Entry [{$entry->id}] for Form [{$form->id}]");
        $response->assertSee('fw-bold">test:</div>', false);
        $response->assertSee('<div class="col-md-10 col-sm-12">foo</div>', false);
    }

    public function testViewEntryByFormIdWithoutViewOnlyNoEntryModel(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create(['view_only' => null]);
        $response = $this->actingAs($user)
            ->get('/vf/'.$form->id);
        $response->assertSee("Entry [1] for Form [{$form->id}]");
        $response->assertSee('fw-bold">test:</div>', false);
        $response->assertSee('<div class="col-md-10 col-sm-12"></div>', false);
    }

    public function testViewEntryHasViewOnlyModelNoElements(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $response = $this->actingAs($user)
            ->get('/v/'.$entry->id);
        $response->assertSee("Entry [{$entry->id}] for Form [{$form->id}]");
        $response->assertSee('<div>Test: </div>', false);
    }

    public function testViewEntryHasViewOnlyModelWithElement(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'foo'])->create();
        $response = $this->actingAs($user)
            ->get('/v/'.$entry->id);
        $response->assertSee("Entry [{$entry->id}] for Form [{$form->id}]");
        $response->assertSee('<div>Test: foo</div>', false);
    }

    public function testViewEntryNoModelFound(): void
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->get('/v/1');
        $response->assertSee('Not Found');
        $response->assertStatus(404);
        $this->assertInstanceOf(ModelNotFoundException::class, $response->baseResponse->exception);
    }

    public function testViewEntryWithoutViewOnlyModelNoElements(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create(['view_only' => null]);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $response = $this->actingAs($user)
            ->get('/v/'.$entry->id);
        $response->assertSee("Entry [{$entry->id}] for Form [{$form->id}]");
        $response->assertSee('fw-bold">test:</div>', false);
        $response->assertSee('<div class="col-md-10 col-sm-12"></div>', false);
    }

    public function testViewEntryWithoutViewOnlyModelWithElements(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create(['view_only' => null]);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'foo'])->create();
        $response = $this->actingAs($user)
            ->get('/v/'.$entry->id);
        $response->assertSee("Entry [{$entry->id}] for Form [{$form->id}]");
        $response->assertSee('fw-bold">test:</div>', false);
        $response->assertSee('<div class="col-md-10 col-sm-12">foo</div>', false);
    }

    protected function defineWebRoutes($router)
    {
        $router->get('/v/{id}', [EntryController::class, 'viewEntry']);
        $router->get('/vf/{formId}', [EntryController::class, 'viewEntryByFormId']);
    }
}
