<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\StringyForm\Http\Controllers\Entry\EntryController;
use Smorken\StringyForm\Http\Controllers\Entry\SaveController;
use Smorken\StringyForm\Models\Eloquent\Entry;
use Smorken\StringyForm\Models\Eloquent\EntryData;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\CanStripNonChars;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\HasForms;
use Tests\Smorken\StringyForm\TestBenchTestCase;

class DirectUpdateTest extends TestBenchTestCase
{
    use CanStripNonChars, DatabaseMigrations, HasForms;

    public function testUpdateCheckInputBoolCanCheck(): void
    {
        $user = User::factory()->create();
        $form = $this->getFormFromBladeString("@include('stringyform::_preset.input.g_check_bool', ['name' => 'name', 'title' => 'Name'])",
            []);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => 1])->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<div class="form-check"> <input type="hidden" name="name" value="0"> <input id="name-1" type="checkbox" class="form-check-input" name="name" value="1" checked="checked" /> <label for="name-1" class="form-check-label" > Name </label> </div>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    public function testUpdateCheckInputBoolNotChecked(): void
    {
        $user = User::factory()->create();
        $form = $this->getFormFromBladeString("@include('stringyform::_preset.input.g_check_bool', ['name' => 'name', 'title' => 'Name'])",
            []);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => 0])->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<div class="form-check"> <input type="hidden" name="name" value="0"> <input id="name-1" type="checkbox" class="form-check-input" name="name" value="1" /> <label for="name-1" class="form-check-label" > Name </label> </div>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    public function testUpdateCheckInputMultiCanCheck(): void
    {
        $user = User::factory()->create();
        $form = $this->getFormFromBladeString("@include('stringyform::_preset.input.g_check_multi', ['name' => 'name[]', 'title' => 'Name', 'value' => ['A' => 'a', 'b', 'c']])",
            []);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => ['a', 'c']])->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<fieldset> <legend>Name</legend> <div class="form-check"> <input id="name-a" type="checkbox" class="form-check-input" name="name[]" value="a" checked="checked" /> <label for="name-a" class="form-check-label" > A </label> </div> <div class="form-check"> <input id="name-b" type="checkbox" class="form-check-input" name="name[]" value="b" /> <label for="name-b" class="form-check-label" > b </label> </div> <div class="form-check"> <input id="name-c" type="checkbox" class="form-check-input" name="name[]" value="c" checked="checked" /> <label for="name-c" class="form-check-label" > c </label> </div> </fieldset>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    public function testUpdateCheckInputSingleCanCheck(): void
    {
        $user = User::factory()->create();
        $form = $this->getFormFromBladeString("@include('stringyform::_preset.input.g_check', ['name' => 'name', 'title' => 'Name'])",
            []);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => '1'])->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<div class="form-check"> <input id="name-1" type="checkbox" class="form-check-input" name="name" value="1" checked="checked" /> <label for="name-1" class="form-check-label" > Name </label> </div>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    public function testUpdateRadioInputMultiCanCheck(): void
    {
        $user = User::factory()->create();
        $form = $this->getFormFromBladeString("@include('stringyform::_preset.input.g_radio_multi', ['name' => 'name[]', 'title' => 'Name', 'value' => ['A' => 'a', 'b']])",
            []);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => 'a'])->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<fieldset> <legend>Name</legend> <div class="form-check"> <input id="name-a" type="radio" class="form-check-input" name="name[]" value="a" checked="checked" /> <label for="name-a" class="form-check-label" > A </label> </div> <div class="form-check"> <input id="name-b" type="radio" class="form-check-input" name="name[]" value="b" /> <label for="name-b" class="form-check-label" > b </label> </div> </fieldset>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    public function testUpdateSelectInputCanSelectOption(): void
    {
        $user = User::factory()->create();
        $form = $this->getFormFromBladeString("@include('stringyform::_preset.input.g_select', ['name' => 'name', 'title' => 'Name', 'items' => ['a' => 'A', 'b' => 'B']])",
            []);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => 'b'])->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<div class="form-group"> <label for="name" class="form-label" > Name </label> <select id="name" class="form-select" name="name" > <option value="a" >A</option> <option value="b" selected>B</option> </select> </div>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    public function testUpdateTextAreaInputCanSetValue(): void
    {
        $user = User::factory()->create();
        $form = $this->getFormFromBladeString("@include('stringyform::_preset.input.g_textarea', ['name' => 'name', 'title' => 'Name'])",
            []);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => 'blah blah blah'])->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<div class="form-group"> <label for="name" class="form-label" > Name </label> <textarea id="name" name="name" class="form-control" maxlength="2048" rows="3" >blah blah blah</textarea> </div>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    public function testUpdateTextInputCanSetValue(): void
    {
        $user = User::factory()->create();
        $form = $this->getFormFromBladeString("@include('stringyform::_preset.input.g_input', ['name' => 'name', 'title' => 'Name'])",
            []);
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => 'blah blah blah'])->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<div class="form-group"> <label for="name" class="form-label" > Name </label> <input id="name" type="text" class="form-control" name="name" value="blah blah blah" maxlength="255" /> </div>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    protected function defineWebRoutes($router)
    {
        $router->get('/', [EntryController::class, 'index']);
        $router->get('/u/{id}', [EntryController::class, 'updateEntryDirect']);
        $router->post('/u/{id}', [SaveController::class, 'doUpdateEntry']);
    }
}
