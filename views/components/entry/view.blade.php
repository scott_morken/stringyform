@php use Smorken\Support\Filter; @endphp
@props([
    'form',
    'entry',
    'viewFromString',
    'filter' => new Filter()
])
<div class="stringy-form">
    <h4>Entry [{{ $entry->id }}] for Form [{{ $form->id }}]</h4>
    @if ($form->view_only)
        {{ $viewFromString->view($form->view_only, [
            'form' => $form,
            'entry' => $entry,
            'filter' => $filter,
        ]) }}
    @else
        <div class="card">
            <div class="card-body">
                <div class="card-title">Elements</div>
                @foreach ($form->elements as $element)
                    <div class="row">
                        <div class="col-md-2 col-sm-12 fw-bold">{{ $element }}:</div>
                        <div class="col-md-10 col-sm-12">{{ $entry->elementValue($element) }}</div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</div>
