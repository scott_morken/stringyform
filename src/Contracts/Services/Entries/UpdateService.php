<?php

namespace Smorken\StringyForm\Contracts\Services\Entries;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\StringyForm\Contracts\Models\StringyForm;
use Smorken\StringyForm\Contracts\Storage\Entry;
use Smorken\StringyForm\Contracts\Storage\EntryData;

interface UpdateService extends BaseService
{
    public function getDataProvider(): EntryData;

    public function getFormRetrieveService(): \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService;

    public function getProvider(): Entry;

    public function getValidatorService(): ValidatorService;

    public function update(Request $request, int $id): CreateOrUpdateResult;

    public function updateByFormAndEntry(
        Request $request,
        StringyForm $form,
        \Smorken\StringyForm\Contracts\Models\Entry $entry
    ): CreateOrUpdateResult;

    public function updateByFormId(Request $request, int $formId): CreateOrUpdateResult;
}
