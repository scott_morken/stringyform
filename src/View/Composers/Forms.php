<?php

namespace Smorken\StringyForm\View\Composers;

use Illuminate\Contracts\View\View;
use Smorken\StringyForm\Contracts\Storage\StringyForm;

class Forms
{
    public function __construct(protected StringyForm $formProvider) {}

    public function compose(View $view): void
    {
        $forms = $this->formProvider->all();
        $view->with('forms', $forms);
    }
}
