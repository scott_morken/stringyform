<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\StringyForm\Contracts\Models\Entry;

interface CreateEntryByResponseIdAction extends Action
{
    public function __invoke(int $formId, int $responseId): Entry;
}
