<?php

namespace Tests\Smorken\StringyForm\Unit\Services\Entries;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\StringyForm\Contracts\Services\Entries\CreateService;
use Smorken\StringyForm\Contracts\Services\Forms\RetrieveService;
use Smorken\StringyForm\Contracts\Storage\Entry;
use Smorken\StringyForm\Contracts\Storage\EntryData;
use Smorken\StringyForm\Contracts\Storage\StringyForm;
use Smorken\StringyForm\Validation\RuleProviders\EntryRules;
use Tests\Smorken\StringyForm\Concerns\WithMockConnection;

class CreateServiceTest extends TestCase
{
    use WithMockConnection;

    public function testCreateFormFailsValidation(): void
    {
        $sut = $this->getSut();
        $request = new Request(['foo' => 'bar']);
        $request->setUserResolver(function () {
            return (new \Illuminate\Foundation\Auth\User)->forceFill(['id' => 2]);
        });
        $form = new \Smorken\StringyForm\Models\Eloquent\StringyForm([
            'name' => 'Foo Form',
            'form' => '<form></form>',
            'element_rules' => [
                'foo' => 'required|min:10',
            ],
        ]);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($form);
        $this->expectException(ValidationException::class);
        $entry = $sut->create($request, 1);
    }

    public function testCreateFormPassesValidation(): void
    {
        $sut = $this->getSut();
        $request = new Request(['foo' => 'bar', 'fiz' => 'buz']);
        $request->setUserResolver(function () {
            return (new \Illuminate\Foundation\Auth\User)->forceFill(['id' => 2]);
        });
        $form = (new \Smorken\StringyForm\Models\Eloquent\StringyForm)->forceFill([
            'id' => 1,
            'name' => 'Foo Form',
            'form' => '<form></form>',
            'element_rules' => [
                'foo' => 'required',
                'fiz' => 'required',
            ],
        ]);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($form);
        $entryModel = (new \Smorken\StringyForm\Models\Eloquent\Entry)->forceFill(['id' => 99]);
        $sut->getProvider()->shouldReceive('create')
            ->once()
            ->with(['form_id' => 1, 'created_by' => 2, 'updated_by' => 2])
            ->andReturn($entryModel);
        $elements = [
            new \Smorken\StringyForm\Models\Eloquent\EntryData,
            new \Smorken\StringyForm\Models\Eloquent\EntryData,
        ];
        $sut->getDataProvider()->shouldReceive('create')
            ->once()
            ->with(['entry_id' => 99, 'element_id' => 0, 'element' => 'foo', 'value' => 'bar'])
            ->andReturn($elements[0]);
        $sut->getDataProvider()->shouldReceive('create')
            ->once()
            ->with(['entry_id' => 99, 'element_id' => 0, 'element' => 'fiz', 'value' => 'buz'])
            ->andReturn($elements[1]);
        $result = $sut->create($request, 1);
        $this->assertEquals($form, $result->form);
        $this->assertEquals($entryModel, $result->entry);
        $this->assertSame($elements[0], $result->elements[0]);
        $this->assertSame($elements[1], $result->elements[1]);
    }

    public function testCreateInvalidFormIdIsException(): void
    {
        $sut = $this->getSut();
        $request = new Request(['foo' => 'bar']);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andThrow(new ModelNotFoundException);
        $this->expectException(ModelNotFoundException::class);
        $sut->create($request, 1);
    }

    protected function getSut(): CreateService
    {
        $sfp = m::mock(StringyForm::class);
        $sfp->shouldReceive('getModel')->andReturn(new \Smorken\StringyForm\Models\Eloquent\StringyForm);
        $services = [
            RetrieveService::class => new \Smorken\StringyForm\Services\Forms\RetrieveService(
                $sfp
            ),
            ValidatorService::class => new \Smorken\Service\Services\ValidatorService(new Factory(new Translator(new ArrayLoader,
                'en'))),
        ];
        $ep = m::mock(Entry::class);
        $ep->shouldReceive('validationRules')
            ->andReturn(EntryRules::rules());

        return new \Smorken\StringyForm\Services\Entries\CreateService(
            $ep,
            m::mock(EntryData::class),
            $services
        );
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
