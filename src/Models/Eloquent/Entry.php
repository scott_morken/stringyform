<?php

namespace Smorken\StringyForm\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Eloquent;
use Smorken\StringyForm\Models\Builders\EntryBuilder;

class Entry extends Eloquent implements \Smorken\StringyForm\Contracts\Models\Entry
{
    /** @use HasBuilder<EntryBuilder> */
    use HasBuilder;

    /** @use HasFactory<\Database\Factories\Smorken\StringyForm\Models\Eloquent\EntryFactory> */
    use HasFactory;

    protected static string $builder = EntryBuilder::class;

    protected $fillable = ['form_id', 'created_by', 'updated_by', 'response_id'];

    public function elementByName(string $name): ?\Smorken\StringyForm\Contracts\Models\EntryData
    {
        $name = $this->ensureElementName($name);
        foreach ($this->elements as $element) {
            if ($element->element === $name) {
                return $element;
            }
        }

        return null;
    }

    public function elementHasValue(string $name, string $value): bool
    {
        if (($old = old($name)) !== null) {
            return $old === $value;
        }
        $element = $this->elementByName($name);
        if ($element) {
            return $element->hasValue($value);
        }

        return false;
    }

    public function elementValue(string $name, bool $toString = false): string|array|null
    {
        if (($old = old($name)) !== null) {
            return $old;
        }
        $element = $this->elementByName($name);
        if ($element && $toString) {
            return $element->valueToString();
        }

        return $element?->value;
    }

    public function elements(): HasMany
    {
        return $this->hasMany(EntryData::class);
    }

    public function form(): BelongsTo
    {
        return $this->belongsTo(StringyForm::class, 'form_id');
    }

    public function hasElements(): bool
    {
        return count($this->elements) > 0;
    }

    protected function ensureElementName(string $name): string
    {
        return str_replace(['[', ']'], '', $name);
    }
}
