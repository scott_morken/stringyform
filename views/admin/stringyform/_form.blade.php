@php use Illuminate\Support\HtmlString; @endphp
<div class="form-group">
    <label for="name" class="form-label">Form Name</label>
    <input name="name" id="name" type="text" class="form-control" value="{{ old('name', $model?->name) }}"
           maxlength="255">
</div>
<div class="form-group">
    <label for="form" class="form-label">Form</label>
    <textarea name="form" id="form" class="form-control"
              rows="15">{{ new HtmlString(old('form', $model?->form)) }}</textarea>
</div>
<div class="form-group">
    <label for="view-only" class="form-label">View Only</label>
    <textarea name="view_only" id="view-only" class="form-control"
              rows="15">{{ new HtmlString(old('view_only', $model?->view_only)) }}</textarea>
</div>
<div class="form-group">
    <label for="element-rules" class="form-label">Rules and Elements</label>
    <textarea name="element_rules" id="element-rules" class="form-control"
              rows="10">{{ new HtmlString(old('element_rules', $model?->element_rules->forInput())) }}</textarea>
</div>
