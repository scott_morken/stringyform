@php
    $name = $name ?? \Smorken\StringyForm\Tools\ElementHelper::name('input_checkbox');
    $id = $id ?? \Smorken\StringyForm\Tools\ElementHelper::ensureId($name.'-'.($value ?? 1));
    $value = $value ?? 1;
    $attrs = [
        'id' => $id,
        'type' => 'checkbox',
        'class' => 'form-check-input '.($classes ?? ''),
        'name' => $name,
        'value' => $value,
        'checked' => (bool) ($checked ?? old($name, isset($entry) ? $entry->elementHasValue($name, $value) : false)),
    ];
@endphp
<input {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!} {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($add_attrs ?? [])) !!}/>
