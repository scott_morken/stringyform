<?php

namespace Smorken\StringyForm\Services\Forms\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \Smorken\StringyForm\Contracts\Services\Forms\Admin\IndexService {}
