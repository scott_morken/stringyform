<?php

namespace Database\Factories\Smorken\StringyForm\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\StringyForm\Models\Eloquent\Entry;

/**
 * @extends Factory<Entry>
 */
class EntryFactory extends Factory
{
    protected $model = Entry::class;

    public function definition(): array
    {
        $userId = $this->faker->randomNumber(4);

        return [
            'form_id' => $this->faker->randomNumber(2),
            'response_id' => 0,
            'created_by' => $userId,
            'updated_by' => $userId,
        ];
    }
}
