<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Repositories;

use Smorken\Domain\Repositories\EloquentIterableRepository;
use Smorken\StringyForm\Contracts\Models\StringyForm;

/**
 * @extends EloquentIterableRepository<\Smorken\StringyForm\Models\Eloquent\StringyForm>
 */
class ActiveFormsRepository extends EloquentIterableRepository implements \Smorken\StringyForm\Contracts\Repositories\ActiveFormsRepository
{
    public function __construct(StringyForm $model)
    {
        parent::__construct($model);
    }
}
