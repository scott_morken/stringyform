<?php
/**
 * @var \Smorken\StringyForm\Contracts\Models\StringyForm $model
 */
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter) ? $filter->all() : []);
?>
@extends(\Illuminate\Support\Facades\Config::get('stringyform.layout', 'layouts.app'))
@section('content')
    @include('stringyform::_preset.controller._to_index')
    <h4>Form #{{ $model->getKey() }}</h4>
    <div class="row mb-4">
        <div class="col">
            <a href="{{ action([$controller, 'update'], $params) }}" title="Update {{ $model->getKey() }}"
               class="btn btn-primary btn-block w-100">Update</a>
        </div>
        <div class="col">
            <a href="{{ action([$controller, 'delete'], $params) }}" title="Delete {{ $model->getKey() }}"
               class="btn btn-danger btn-block w-100">Delete</a>
        </div>
    </div>
    <div class="mb-2">
        <div class="row">
            <div class="col-md-2 col-sm-12 font-weight-bold fw-bold">ID</div>
            <div class="col-md-10 col-sm-12">{{ $model->getKey() }}</div>
        </div>
        <div class="row">
            <div class="col-md-2 col-sm-12 font-weight-bold fw-bold">Name</div>
            <div class="col-md-10 col-sm-12">{{ $model->name }}</div>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-body">
            <div class="card-title">
                Form
                <a href="{{ action([$controller, 'preview'], $params) }}" class="float-end"
                   title="Preview Form">Preview</a>
            </div>
            <pre>{{ $model->form }}</pre>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-body">
            <div class="card-title">View</div>
            <pre>{{ $model->view_only }}</pre>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-body">
            <div class="card-title">Elements and Rules</div>
            @foreach ($model->element_rules->toArray() as $element => $rule)
                <div class="row">
                    <div class="col-md-2 col-sm-12 font-weight-bold fw-bold">{{ $element }}</div>
                    <div class="col-md-10 col-sm-12">{{ $rule }}</div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
