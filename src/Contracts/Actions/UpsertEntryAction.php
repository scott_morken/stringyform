<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\UpsertAction;

interface UpsertEntryAction extends UpsertAction {}
