<?php

namespace Smorken\StringyForm\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * @property int $id
 * @property int $form_id
 * @property int $response_id
 * @property int $created_by
 * @property int $updated_by
 *
 * Relations
 * @property ?\Smorken\StringyForm\Contracts\Models\StringyForm $form
 * @property \Illuminate\Support\Collection<\Smorken\StringyForm\Contracts\Models\EntryData> $elements
 *
 * @phpstan-require-extends \Smorken\StringyForm\Models\Eloquent\Entry
 */
interface Entry extends Model
{
    public function elementByName(string $name): ?EntryData;

    public function elementHasValue(string $name, string $value): bool;

    public function elementValue(string $name, bool $toString = false): string|array|null;

    public function hasElements(): bool;
}
