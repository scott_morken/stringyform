<?php

namespace Smorken\StringyForm\Services\Entries;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \Smorken\StringyForm\Contracts\Services\Entries\IndexService {}
