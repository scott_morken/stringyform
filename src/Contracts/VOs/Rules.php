<?php

namespace Smorken\StringyForm\Contracts\VOs;

/**
 * @phpstan-require-extends \Smorken\StringyForm\VOs\Rules
 */
interface Rules extends \Stringable
{
    public function __construct(array|string|null $input = null);

    public function elements(): array;

    public function forInput(): string;

    public function toArray(): array;

    public static function fromInput(array|string $input): static;
}
