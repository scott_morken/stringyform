<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Actions;

use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\StringyForm\Contracts\Models\Entry;

/**
 * @extends EloquentUpsertAction<\Smorken\StringyForm\Models\Eloquent\Entry, \Smorken\Domain\Actions\Upsertable>
 */
class UpsertEntryAction extends EloquentUpsertAction implements \Smorken\StringyForm\Contracts\Actions\UpsertEntryAction
{
    public function __construct(Entry $model)
    {
        parent::__construct($model);
    }
}
