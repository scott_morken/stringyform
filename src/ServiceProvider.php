<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 8:40 AM
 */

namespace Smorken\StringyForm;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Facades\View;
use Smorken\Data\DataGatePolicyBuilder;
use Smorken\Service\Invokables\Invoker;
use Smorken\Storage\Contracts\Base;
use Smorken\StringyForm\Actions\CreateElementsAction;
use Smorken\StringyForm\Contracts\Storage\Entry;
use Smorken\StringyForm\Contracts\Storage\EntryData;
use Smorken\StringyForm\Contracts\Storage\StringyForm;
use Smorken\StringyForm\Models\Observers\CreatedByUpdatedByObserver;
use Smorken\StringyForm\Providers\Services\EntryServices;
use Smorken\StringyForm\Providers\Services\StringyFormServices;
use Smorken\StringyForm\Tools\AuthorizeUserForEntry;
use Smorken\StringyForm\View\Composers\Forms;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    protected array $invokableServices = [
        'invokables' => [
            EntryServices::class,
            StringyFormServices::class,
            \Smorken\StringyForm\Providers\Services\Admin\EntryServices::class,
            \Smorken\StringyForm\Providers\Services\Admin\StringyFormServices::class,
        ],
    ];

    protected array $viewComposers = [
        Forms::class => [
            'stringyform::admin.entry._filter_form',
            'stringyform::admin.entry._form',
        ],
    ];

    public function boot(): void
    {
        $this->bootConfig();
        $this->bootViews();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
        $this->bootViewComposers();
        \Smorken\StringyForm\Models\Eloquent\Entry::observe(CreatedByUpdatedByObserver::class);
        CreateElementsAction::setValidationFactory($this->app[Factory::class]);
        AuthorizeUserForEntry::setGuardResolver(fn () => $this->app[Guard::class]);
    }

    public function register(): void
    {
        $this->registerStorageProviders();
        $this->registerInvokables($this->invokableServices);
        $this->registerActions();
        $this->registerModels();
        $this->registerRepositories();
        $this->booting(function () {
            $this->registerGatePolicies();
        });
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'stringyform');
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('stringyform.php'),
        ], 'config');
    }

    protected function bootViewComposers(): void
    {
        foreach ($this->viewComposers as $viewComposer => $views) {
            View::composer($views, $viewComposer);
        }
    }

    protected function bootViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'stringyform');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('/views/vendor/smorken/stringyform'),
            ],
            'views'
        );
    }

    protected function registerActions(): void
    {
        foreach ($this->app['config']->get('stringyform.actions', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }

    protected function registerGatePolicies(): void
    {
        $builder = new DataGatePolicyBuilder($this->app[Gate::class], $this->app);
        $builder->create($this->app['config']->get('stringyform.policies', []));
    }

    protected function registerInvokables(array $invokables): void
    {
        $invoker = new Invoker($invokables, app_path());
        $invoker->handle($this->app);
    }

    protected function registerModels(): void
    {
        foreach ($this->app['config']->get('stringyform.models', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }

    protected function registerRepositories(): void
    {
        foreach ($this->app['config']->get('stringyform.repositories', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }

    protected function registerStorageProvider(string $interfaceClass, array $providers): ?Base
    {
        $providerData = $providers[$interfaceClass] ?? null;
        if ($providerData) {
            $providerClass = $providerData['impl'];
            $modelClass = $providerData['model'];
            if ($providerClass && $modelClass) {
                return new $providerClass(new $modelClass);
            }
        }

        return null;
    }

    protected function registerStorageProviders(): void
    {
        $this->app->bind(Entry::class, function ($app) {
            $providers = $app['config']->get('stringyform.providers', []);

            return $this->registerStorageProvider(Entry::class, $providers);
        });
        $this->app->bind(EntryData::class, function ($app) {
            $providers = $app['config']->get('stringyform.providers', []);

            return $this->registerStorageProvider(EntryData::class, $providers);
        });
        $this->app->bind(StringyForm::class, function ($app) {
            $providers = $app['config']->get('stringyform.providers', []);

            return $this->registerStorageProvider(StringyForm::class, $providers);
        });
    }
}
