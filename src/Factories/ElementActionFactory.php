<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Factories;

use Illuminate\Support\Collection;
use Smorken\Domain\Factories\ActionFactory;
use Smorken\StringyForm\Contracts\Actions\CreateElementsAction;
use Smorken\StringyForm\Contracts\Actions\Results\CreateElementsResult;
use Smorken\StringyForm\Contracts\Models\Entry;

class ElementActionFactory extends ActionFactory
{
    public function createElements(Collection $elements, Entry $entry): CreateElementsResult
    {
        return $this->for(CreateElementsAction::class, ['elements' => $elements, 'entry' => $entry]);
    }
}
