<?php

declare(strict_types=1);

namespace Smorken\StringyForm\View\Composers;

use Illuminate\Contracts\View\View;
use Smorken\StringyForm\Factories\FormRepositoryFactory;
use Smorken\StringyForm\ViewModels\ActiveFormsViewModel;

class ActiveForms
{
    public function __construct(protected FormRepositoryFactory $formRepositoryFactory) {}

    public function compose(View $view): void
    {
        $view->with('activeFormsViewModel', new ActiveFormsViewModel($this->formRepositoryFactory->active()));
    }
}
