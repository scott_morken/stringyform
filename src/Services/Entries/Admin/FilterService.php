<?php

namespace Smorken\StringyForm\Services\Entries\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_formId' => $request->input('f_formId'),
            'f_userId' => $request->input('f_userId'),
        ]);
    }
}
