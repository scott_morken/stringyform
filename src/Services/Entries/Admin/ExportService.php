<?php

namespace Smorken\StringyForm\Services\Entries\Admin;

use Smorken\Export\Contracts\Export;
use Smorken\Service\Services\ExportByStorageProviderService;
use Smorken\StringyForm\Contracts\Models\Entry;
use Smorken\StringyForm\Contracts\Models\StringyForm;

class ExportService extends ExportByStorageProviderService implements \Smorken\StringyForm\Contracts\Services\Entries\Admin\ExportService
{
    protected function getElements(Entry $model): array
    {
        $elements = [];
        foreach ($this->getFormElements($model->form) as $element) {
            $elements[] = $model->elementValue($element, true);
        }

        return $elements;
    }

    protected function getFormElements(?StringyForm $form): array
    {
        return $form?->elements ?? [];
    }

    protected function getHeaderCallback(...$params): ?callable
    {
        return fn (Export $exporter, array $headers, ?Entry $firstModel): array => array_merge([
            'id', 'created_by', 'updated_by', 'form_id', 'form_name',
        ], $this->getFormElements($firstModel?->form));
    }

    protected function getRowCallback(...$params): ?callable
    {
        return function (Export $exporter, array $data, Entry $model): array {
            $row = [
                'id' => $model->id,
                'created_by' => $model->created_by,
                'updated_by' => $model->updated_by,
                'form_id' => $model->form_id,
                'form_name' => $model->form?->name,
            ] + $this->getElements($model);

            return [
                $row,
            ];
        };
    }
}
