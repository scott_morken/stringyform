<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\Domain\Actions\Contracts\Results\DeleteResult;

interface DeleteEntryByResponseIdAction extends Action
{
    public function __invoke(int $responseId): DeleteResult;
}
