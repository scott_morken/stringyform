@php
$attrs = $attrs ?? [];
@endphp
{!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!}
