<?php

namespace Smorken\StringyForm\Storage\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Smorken\StringyForm\Validation\RuleProviders\EntryRules;

class Entry extends Base implements \Smorken\StringyForm\Contracts\Storage\Entry
{
    public function delete($model): bool
    {
        $model->elements()->delete();

        return parent::delete($model);
    }

    public function findFirstByFormId(
        int $formId,
        int $userId,
        bool $shouldCreate = true
    ): ?\Smorken\StringyForm\Contracts\Models\Entry {
        $m = $this->getModel()
            ->newQuery()
            ->formIdIs($formId)
            ->createdByIs($userId)
            ->defaultOrder()
            ->first();
        if ($m) {
            return $m;
        }
        if ($shouldCreate) {
            return $this->create(['form_id' => $formId, 'created_by' => $userId, 'updated_by' => $userId]);
        }

        return null;
    }

    public function setUpdatedBy(
        \Smorken\StringyForm\Contracts\Models\Entry $entry,
        int $userId
    ): \Smorken\StringyForm\Contracts\Models\Entry {
        $entry->updated_by = $userId;
        $entry->save();

        return $entry;
    }

    public function validationRules(array $override = []): array
    {
        return [...EntryRules::rules(), ...$override];
    }

    protected function filterFormId(Builder $query, $v): Builder
    {
        if (strlen($v ?? '')) {
            // @phpstan-ignore method.notFound
            $query->formIdIs($v);
        }

        return $query;
    }

    protected function filterUserId(Builder $query, $v): Builder
    {
        if (strlen($v ?? '')) {
            // @phpstan-ignore method.notFound
            $query->createdByIs($v);
        }

        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_formId' => 'filterFormId',
            'f_userId' => 'filterUserId',
        ];
    }
}
