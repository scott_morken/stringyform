<?php

namespace Tests\Smorken\StringyForm\Unit\Tools;

use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Tools\HtmlAttribute;

class HtmlAttributeTest extends TestCase
{
    public function testFalseValueIsNotRenderable(): void
    {
        $sut = new HtmlAttribute('test', false);
        $this->assertFalse($sut->shouldRender());
        $this->assertEquals('', (string) $sut);
    }

    public function testNullValueIsKeyWithoutValue(): void
    {
        $sut = new HtmlAttribute('test', null);
        $this->assertTrue($sut->shouldRender());
        $this->assertEquals('test', (string) $sut);
    }

    public function testStringValue(): void
    {
        $sut = new HtmlAttribute('test', 'foo');
        $this->assertTrue($sut->shouldRender());
        $this->assertEquals('test="foo"', (string) $sut);
    }

    public function testTrueValueIsKey(): void
    {
        $sut = new HtmlAttribute('test', true);
        $this->assertTrue($sut->shouldRender());
        $this->assertEquals('test="test"', (string) $sut);
    }
}
