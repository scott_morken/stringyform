<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Validation\RuleProviders;

class EntryRules
{
    public static function rules(): array
    {
        return [
            'form_id' => 'required|int|min:1',
            'created_by' => 'required|int',
            'updated_by' => 'required|int',
        ];
    }
}
