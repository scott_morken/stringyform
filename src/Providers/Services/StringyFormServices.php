<?php

namespace Smorken\StringyForm\Providers\Services;

use Smorken\Service\Invokables\Invokable;
use Smorken\StringyForm\Contracts\Services\Forms\RetrieveService;
use Smorken\StringyForm\Contracts\Storage\StringyForm;

class StringyFormServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(RetrieveService::class, fn ($app) => new \Smorken\StringyForm\Services\Forms\RetrieveService($app[StringyForm::class]));
    }
}
