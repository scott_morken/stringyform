<?php

namespace Smorken\StringyForm\Http\Controllers\Entry;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithService\Concerns\HasIndex;
use Smorken\String2Blade\Contracts\Support\ViewFromString;
use Smorken\StringyForm\Contracts\Models\Entry;
use Smorken\StringyForm\Contracts\Models\StringyForm;
use Smorken\StringyForm\Contracts\Services\Entries\EntryFactory;
use Smorken\StringyForm\Contracts\Services\Entries\IndexService;

class EntryController extends BaseController
{
    use HasIndex;

    public function __construct(
        EntryFactory $entryFactory,
        IndexService $indexService,
        protected ViewFromString $viewFromString
    ) {
        $this->indexService = $indexService;
        parent::__construct($entryFactory);
    }

    public function createEntry(Request $request, int $formId): View
    {
        $formResult = $this->entryFactory->getFormRetrieveService()->findById($formId);

        // @phpstan-ignore property.notFound
        return $this->handleForm($request, $formResult->model, $this->entryFactory->getRetrieveService()->getModel(),
            false);
    }

    public function createEntryDirect(Request $request, int $formId): View
    {
        $formResult = $this->entryFactory->getFormRetrieveService()->findById($formId);

        // @phpstan-ignore property.notFound
        return $this->handleForm($request, $formResult->model, $this->entryFactory->getRetrieveService()->getModel(),
            true);
    }

    public function updateEntry(Request $request, int $id): View
    {
        $entryResult = $this->entryFactory->getRetrieveService()->findById($id);

        // @phpstan-ignore property.notFound, property.notFound
        return $this->handleForm($request, $entryResult->model->form, $entryResult->model, false);
    }

    public function updateEntryByFormId(Request $request, int $formId): View
    {
        $formResult = $this->entryFactory->getFormRetrieveService()->findById($formId);
        $entryResult = $this->entryFactory->getRetrieveService()->findByFormId($request, $formId);

        // @phpstan-ignore property.notFound, argument.type
        return $this->handleForm($request, $formResult->model, $entryResult->model, false);
    }

    public function updateEntryByFormIdDirect(Request $request, int $formId): View
    {
        $formResult = $this->entryFactory->getFormRetrieveService()->findById($formId);
        $entryResult = $this->entryFactory->getRetrieveService()->findByFormId($request, $formId);

        // @phpstan-ignore property.notFound, argument.type
        return $this->handleForm($request, $formResult->model, $entryResult->model, true);
    }

    public function updateEntryDirect(Request $request, int $id): View
    {
        $entryResult = $this->entryFactory->getRetrieveService()->findById($id);

        // @phpstan-ignore property.notFound, property.notFound
        return $this->handleForm($request, $entryResult->model->form, $entryResult->model, true);
    }

    public function viewEntry(Request $request, int $id): View
    {
        $entryResult = $this->entryFactory->getRetrieveService()->findById($id);

        // @phpstan-ignore property.notFound, property.notFound
        return $this->handleView($request, $entryResult->model->form, $entryResult->model, false);
    }

    public function viewEntryByFormId(Request $request, int $formId): View
    {
        $formResult = $this->entryFactory->getFormRetrieveService()->findById($formId);
        $entryResult = $this->entryFactory->getRetrieveService()->findByFormId($request, $formId);

        // @phpstan-ignore property.notFound, argument.type
        return $this->handleView($request, $formResult->model, $entryResult->model, false);
    }

    public function viewEntryByFormIdDirect(Request $request, int $formId): View
    {
        $formResult = $this->entryFactory->getFormRetrieveService()->findById($formId);
        $entryResult = $this->entryFactory->getRetrieveService()->findByFormId($request, $formId);

        // @phpstan-ignore property.notFound, argument.type
        return $this->handleView($request, $formResult->model, $entryResult->model, true);
    }

    public function viewEntryDirect(Request $request, int $id): View
    {
        $entryResult = $this->entryFactory->getRetrieveService()->findById($id);

        // @phpstan-ignore property.notFound, property.notFound
        return $this->handleView($request, $entryResult->model->form, $entryResult->model, true);
    }

    protected function getFormView(StringyForm $form, bool $string2Blade = false): View
    {
        if ($string2Blade) {
            return $this->viewFromString->view($form->form);
        }

        return $this->makeView($this->getViewName('form'));
    }

    protected function getViewView(StringyForm $form, bool $string2Blade = false): View
    {
        if ($string2Blade && $form->view_only) {
            return $this->viewFromString->view($form->view_only);
        }

        return $this->makeView($this->getViewName('view'));
    }

    protected function handleForm(Request $request, StringyForm $form, Entry $entry, bool $string2Blade = false): View
    {
        return $this->getFormView($form, $string2Blade)
            ->with('filter', $this->getFilter($request))
            ->with('form', $form)
            ->with('entry', $entry)
            ->with('viewFromString', $this->viewFromString);
    }

    protected function handleView(Request $request, StringyForm $form, Entry $entry, bool $string2Blade = false): View
    {
        return $this->getViewView($form, $string2Blade)
            ->with('filter', $this->getFilter($request))
            ->with('form', $form)
            ->with('entry', $entry)
            ->with('viewFromString', $this->viewFromString);
    }
}
