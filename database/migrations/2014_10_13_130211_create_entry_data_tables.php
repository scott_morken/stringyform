<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends \Illuminate\Database\Migrations\Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = ['entries', 'entry_data'];
        foreach ($tables as $t) {
            Schema::dropIfExists($t);
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $t) {
            $t->increments('id');
            $t->integer('form_id')->unsigned();
            $t->timestamps();
            $t->integer('created_by')->unsigned();
            $t->integer('updated_by')->unsigned();

            $t->index('form_id', 'en_form_id_ndx');
            $t->index('created_by', 'en_created_by_ndx');
        });

        Schema::create('entry_data', function (Blueprint $t) {
            $t->increments('id');
            $t->integer('entry_id')->unsigned();
            $t->integer('element_id')->unsigned();
            $t->text('value')->nullable();
            $t->timestamps();

            $t->index('entry_id', 'ed_entry_id_ndx');
            $t->index('element_id', 'ed_element_id_ndx');
        });
    }
};
