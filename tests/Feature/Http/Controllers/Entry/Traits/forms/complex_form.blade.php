<form method="post">
    @include('stringyform::_preset.input.g_input', ['name' => 'name', 'title' => 'Name'])
    @include('stringyform::_preset.input.g_select', [
        'name' => 'location',
        'title' => 'Location',
        'items' => ['' => 'Select one', 'a' => 'Location A', 'b' => 'Location B'],
    ])
    @include('stringyform::_preset.input.g_check_bool', ['name' => 'active', 'title' => 'Active'])
    <fieldset>
        <legend>Favorite Colors</legend>
        @include('stringyform::_preset.input.g_check', ['name' => 'fave_colors[]', 'title' => 'Blue', 'value' => 'blue'])
        @include('stringyform::_preset.input.g_check', ['name' => 'fave_colors[]', 'title' => 'Green', 'value' => 'green'])
        @include('stringyform::_preset.input.g_check', ['name' => 'fave_colors[]', 'title' => 'Red', 'value' => 'red'])
    </fieldset>
    <fieldset>
        <legend>Best University</legend>
        @include('stringyform::_preset.input.g_radio', ['name' => 'uni', 'title' => 'ASU', 'value' => 'asu'])
        @include('stringyform::_preset.input.g_radio', ['name' => 'uni', 'title' => 'UofA', 'value' => 'arizona'])
    </fieldset>
    <div>
        @include('stringyform::_preset.input._button', ['title' => 'Save', 'name' => 'submit_form'])
        @include('stringyform::_preset.input._anchor', ['title' => 'Back', 'id' => 'back-link', 'href' => action([\Smorken\StringyForm\Http\Controllers\Entry\EntryController::class, 'index'])])
    </div>
</form>
