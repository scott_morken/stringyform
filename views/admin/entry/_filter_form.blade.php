<?php $filtered = 'border border-success'; ?>
<div class="card my-2 mt-2 mb-2">
    <div class="card-body">
        <form class="row g-3 align-items-center form-inline" method="get">
            <div class="col-md form-group mb-2 mr-2">
                @include('stringyform::_preset.input._label', ['name' => 'f_formId', 'title' => 'Form', 'label_classes' => 'sr-only visually-hidden'])
                @include('stringyform::_preset.input._select', [
                'name' => 'f_formId',
                'classes' => $filter->f_formId ? $filtered : '',
                'value' => $filter->f_formId,
                'items' => ['' => '-- Select One --'] + $forms->pluck('name', 'id')->all()
                ])
            </div>
            <div class="col-md form-group mb-2 mr-2">
                @include('stringyform::_preset.input._label', ['name' => 'f_userId', 'title' => 'User ID', 'label_classes' => 'visually-hidden'])
                @include('stringyform::_preset.input._input', [
                'name' => 'f_userId',
                'classes' => $filter->f_userId ? $filtered : '',
                'value' => $filter->f_userId,
                'placeholder' => 'User ID (3...)'
                ])
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
            </div>
        </form>
        <div>
            @include('stringyform::_preset.input._anchor', ['title' => 'Export', 'href' => action([$controller, 'export'], $filter->except(['page'])), 'classes' => 'btn btn-success'])
            <small>Filter your results <i>before</i> exporting.</small>
        </div>
    </div>
</div>
