<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Actions\Results;

use Illuminate\Contracts\Support\MessageBag;

class CreateElementsResult implements \Smorken\StringyForm\Contracts\Actions\Results\CreateElementsResult
{
    public MessageBag $messages;

    public function __construct(
        public int $entryId,
        public int $created,
        public int $updated,
        public int $deleted,
        ?MessageBag $messages = null
    ) {
        if ($messages === null) {
            $messages = new \Illuminate\Support\MessageBag;
        }
        $this->messages = $messages;
    }
}
