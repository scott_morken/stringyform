<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Validation\RuleProviders;

class EntryDataRules
{
    public static function rules(): array
    {
        return [
            'entry_id' => 'required|integer|min:1',
            'element' => 'required|string|max:255',
        ];
    }
}
