<?php

namespace Smorken\StringyForm\Services\Entries;

use Smorken\Service\Services\Factory;
use Smorken\StringyForm\Contracts\Services\Entries\CreateService;
use Smorken\StringyForm\Contracts\Services\Entries\UpdateService;

class SaveFactory extends Factory implements \Smorken\StringyForm\Contracts\Services\Entries\SaveFactory
{
    protected array $services = [
        \Smorken\StringyForm\Contracts\Services\Entries\CreateService::class => null,
        \Smorken\StringyForm\Contracts\Services\Entries\UpdateService::class => null,
    ];

    public function getCreateService(): CreateService
    {
        /** @var CreateService */
        return $this->getService(CreateService::class);
    }

    public function getUpdateService(): UpdateService
    {
        /** @var UpdateService */
        return $this->getService(UpdateService::class);
    }
}
