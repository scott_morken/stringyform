<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Admin;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Smorken\Roles\ServiceProvider;
use Smorken\Roles\Support\DefineGates;
use Smorken\StringyForm\Models\Eloquent\Entry;
use Smorken\StringyForm\Models\Eloquent\EntryData;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\CanStripNonChars;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\HasForms;
use Tests\Smorken\StringyForm\TestBenchTestCase;

class EntryControllerTest extends TestBenchTestCase
{
    use CanStripNonChars, DatabaseMigrations, HasForms;

    public function testExportWithEntriesNoFormSet(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form1 = $this->getComplexForm();
        $entry1 = Entry::factory(['form_id' => $form1->id])->create();
        $form2 = $this->getSimpleForm();
        $entry2 = Entry::factory(['form_id' => $form2->id])->create();
        $response = $this->actingAs($user)
            ->get('/admin/entry/export');
        $response->assertRedirect('/admin/entry');
    }

    public function testExportWithEntriesWithElements(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form1 = $this->getComplexForm();
        $entry1 = Entry::factory(['form_id' => $form1->id])->create();
        $form2 = $this->getSimpleForm();
        $entry2 = Entry::factory(['form_id' => $form2->id])->create();
        $elements = [
            EntryData::factory(['entry_id' => $entry1->id, 'element' => 'name', 'value' => 'blah blah'])->create(),
            EntryData::factory(['entry_id' => $entry1->id, 'element' => 'active', 'value' => '1'])->create(),
            EntryData::factory(['entry_id' => $entry1->id, 'element' => 'location', 'value' => 'b'])->create(),
            EntryData::factory(['entry_id' => $entry1->id, 'element' => 'fave_colors', 'value' => ['blue', 'green']])
                ->create(),
            EntryData::factory(['entry_id' => $entry1->id, 'element' => 'uni', 'value' => 'asu'])->create(),
        ];
        $response = $this->actingAs($user)
            ->get('/admin/entry/export?f_formId='.$form1->id);
        $response->assertSee('id,created_by,updated_by,form_id,form_name,name,location,active,fave_colors,uni', false);
        $response->assertSee(sprintf('%d,%d,%d,%d,"%s","blah blah",b,1,"blue, green",asu', $entry1->id,
            $entry1->created_by, $entry1->updated_by,
            $entry1->form_id, $form1->name), false);
    }

    public function testExportWithEntriesWithNoElements(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form1 = $this->getComplexForm();
        $entry1 = Entry::factory(['form_id' => $form1->id])->create();
        $form2 = $this->getSimpleForm();
        $entry2 = Entry::factory(['form_id' => $form2->id])->create();
        $response = $this->actingAs($user)
            ->get('/admin/entry/export?f_formId='.$form1->id);
        $response->assertSee('id,created_by,updated_by,form_id,form_name,name,location,active,fave_colors,uni', false);
        $response->assertSee(sprintf('%d,%d,%d,%d,"%s",,,,,', $entry1->id, $entry1->created_by, $entry1->updated_by,
            $entry1->form_id, $form1->name), false);
    }

    public function testIndexNoRecords(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $response = $this->actingAs($user)
            ->get('/admin/entry');
        $response->assertSee('No records found');
    }

    public function testIndexWithEntries(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form1 = $this->getComplexForm();
        $entry1 = Entry::factory(['form_id' => $form1->id])->create();
        $form2 = $this->getSimpleForm();
        $entry2 = Entry::factory(['form_id' => $form2->id])->create();
        $response = $this->actingAs($user)
            ->get('/admin/entry');
        $response->assertDontSee('No records found');
        $response->assertSee('<td>'.$form1->name.'</td>', false);
        $response->assertSee('<td>'.$form2->name.'</td>', false);
        $response->assertSee('/admin/entry/view/'.$entry1->id);
        $response->assertSee('/admin/entry/view/'.$entry2->id);
    }

    public function testIndexWithEntriesFilterByForm(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form1 = $this->getComplexForm();
        $entry1 = Entry::factory(['form_id' => $form1->id])->create();
        $form2 = $this->getSimpleForm();
        $entry2 = Entry::factory(['form_id' => $form2->id])->create();
        $response = $this->actingAs($user)
            ->get('/admin/entry?f_formId='.$form1->id);
        $response->assertDontSee('No records found');
        $response->assertSee('<td>'.$form1->name.'</td>', false);
        $response->assertDontSee('<td>'.$form2->name.'</td>', false);
        $response->assertSee('/admin/entry/view/'.$entry1->id);
        $response->assertDontSee('/admin/entry/view/'.$entry2->id);
    }

    public function testNonAdminUserIsNotAuthorized(): void
    {
        $user = User::factory(['id' => '12345'])->create();
        $response = $this->actingAs($user)
            ->get('/admin/entry');
        $response->assertStatus(403);
        $response->assertSee('This action is unauthorized');
    }

    public function testViewWithForm(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form = $this->getComplexForm();
        $entry = Entry::factory(['form_id' => $form->id])->create();
        $elements = [
            EntryData::factory(['entry_id' => $entry->id, 'element' => 'name', 'value' => 'blah blah'])->create(),
            EntryData::factory(['entry_id' => $entry->id, 'element' => 'active', 'value' => '1'])->create(),
            EntryData::factory(['entry_id' => $entry->id, 'element' => 'location', 'value' => 'b'])->create(),
            EntryData::factory(['entry_id' => $entry->id, 'element' => 'fave_colors', 'value' => ['blue', 'green']])
                ->create(),
            EntryData::factory(['entry_id' => $entry->id, 'element' => 'uni', 'value' => 'asu'])->create(),
        ];
        $response = $this->actingAs($user)
            ->get('/admin/entry/view/'.$entry->id);
        $response->assertSee(sprintf('%d: %s', $form->id, $form->name));
        $response->assertSee('col-sm-12">blah blah<', false);
        $response->assertSee('col-sm-12">b<', false);
        $response->assertSee('col-sm-12">1<', false);
        $response->assertSee('col-sm-12">blue, green<', false);
        $response->assertSee('col-sm-12">asu<', false);
    }

    public function testViewWithNoEntry(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $response = $this->actingAs($user)
            ->get('/admin/entry/view/99');
        $response->assertStatus(404);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RoleSeeder::class);
        RoleUser::create(['user_id' => '1', 'role_id' => 1]);
        ServiceProvider::defineGates($this->app, true);
        DefineGates::reset(true);
    }
}
