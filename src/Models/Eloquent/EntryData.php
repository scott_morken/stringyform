<?php

namespace Smorken\StringyForm\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Concerns\WithDefaultScopes;
use Smorken\Model\Eloquent;
use Smorken\StringyForm\Models\Builders\EntryDataBuilder;

class EntryData extends Eloquent implements \Smorken\StringyForm\Contracts\Models\EntryData
{
    /** @use HasBuilder<EntryDataBuilder> */
    use HasBuilder;

    /** @use HasFactory<\Database\Factories\Smorken\StringyForm\Models\Eloquent\EntryDataFactory> */
    use HasFactory;

    use WithDefaultScopes;

    protected static string $builder = EntryDataBuilder::class;

    protected $attributes = [
        'element_id' => 0,
    ];

    protected $casts = ['value' => 'json'];

    protected $fillable = [
        'entry_id',
        'element_id',
        'element',
        'value',
    ];

    protected $table = 'entry_data';

    public function entry(): BelongsTo
    {
        return $this->belongsTo(Entry::class);
    }

    public function hasValue(string $value): bool
    {
        $attributeValue = $this->getAttribute('value');
        if (is_array($attributeValue)) {
            return in_array($value, $attributeValue);
        }

        return $value === (string) $attributeValue;
    }

    public function valueToString(): ?string
    {
        $value = $this->getAttribute('value');
        if (is_array($value)) {
            return implode(', ', $value);
        }

        return (string) $value;
    }
}
