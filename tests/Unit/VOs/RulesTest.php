<?php

namespace Tests\Smorken\StringyForm\Unit\VOs;

use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\VOs\Rules;

class RulesTest extends TestCase
{
    public function testForInput(): void
    {
        $expected = <<<'RULES'
foo=>required|int|min:1
bar=>optional|max:64
fiz=>int
buz=>
RULES;
        $rulesArray = [
            'foo' => 'required|int|min:1',
            'bar' => 'optional|max:64',
            'fiz' => 'int',
            'buz' => null,
        ];
        $rules = new Rules($rulesArray);
        $this->assertEquals($expected, $rules->forInput());
    }

    public function testFromJsonStringInput(): void
    {
        $ruleString = '{"foo":"required|int|min:1","bar":"optional|max:64","fiz":"int"}';
        $expected = [
            'foo' => 'required|int|min:1',
            'bar' => 'optional|max:64',
            'fiz' => 'int',
        ];
        $this->assertEquals($expected, (new Rules($ruleString))->toArray());
    }

    public function testFromMixedInput(): void
    {
        $ruleInput = [
            'foo' => 'required|int|min:1',
            'bar=>optional|max:64',
            'fiz' => 'int',
        ];
        $expected = [
            'foo' => 'required|int|min:1',
            'bar' => 'optional|max:64',
            'fiz' => 'int',
        ];
        $this->assertEquals($expected, (new Rules($ruleInput))->toArray());
    }

    public function testFromStringInput(): void
    {
        $ruleString = <<<'RULES'
foo=>required|int|min:1
bar=>optional|max:64
fiz => int
buz=>
RULES;
        $expected = [
            'foo' => 'required|int|min:1',
            'bar' => 'optional|max:64',
            'fiz' => 'int',
            'buz' => null,
        ];
        $rules = new Rules($ruleString);
        $this->assertEquals($expected, $rules->toArray());
        $this->assertEquals(array_keys($expected), $rules->elements());
    }

    public function testToString(): void
    {
        $ruleString = <<<'RULES'
foo=>required|int|min:1
bar=>optional|max:64
fiz=>int
RULES;
        $expected = '{"foo":"required|int|min:1","bar":"optional|max:64","fiz":"int"}';
        $this->assertEquals($expected, (string) (new Rules($ruleString)));
    }
}
