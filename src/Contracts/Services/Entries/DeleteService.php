<?php

namespace Smorken\StringyForm\Contracts\Services\Entries;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;

interface DeleteService extends \Smorken\Service\Contracts\Services\DeleteService
{
    public function deleteFromFormId(Request $request, int $formId): ModelResult;
}
