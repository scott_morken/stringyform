<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Tools;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Auth\Guard;
use Smorken\StringyForm\Contracts\Models\Entry;

class AuthorizeUserForEntry
{
    protected static ?Guard $guard = null;

    protected static \Closure $guardResolver;

    public static function authorize(Entry $entry): void
    {
        $userId = self::$guard->user()->getAuthIdentifier();
        if ($userId !== $entry->created_by) {
            throw new AuthorizationException('You are not authorized to access that entry.');
        }
    }

    public static function getGuard(): Guard
    {
        if (self::$guard === null) {
            self::$guard = (self::$guardResolver)();
        }

        return self::$guard;
    }

    public static function setGuardResolver(\Closure $resolver): void
    {
        self::$guardResolver = $resolver;
    }
}
