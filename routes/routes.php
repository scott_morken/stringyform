<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => Config::get('stringyform.admin.prefix', 'admin'),
    'middleware' => Config::get('stringyform.admin.middleware', ['web', 'auth', 'can:role-admin']),
], function () {
    Route::group([
        'prefix' => 'form',
    ], function () {
        $controller = Config::get('stringyform.admin.form_controller',
            \Smorken\StringyForm\Http\Controllers\Admin\StringyFormController::class);
        \Smorken\Support\Routes::create($controller);
        Route::get('preview/{id}',
            [\Smorken\StringyForm\Http\Controllers\Admin\StringyFormController::class, 'preview']);
        Route::post('preview/{id}',
            [\Smorken\StringyForm\Http\Controllers\Admin\StringyFormController::class, 'doPreview']);
    });
    Route::group([
        'prefix' => 'entry',
    ], function () {
        $controller = Config::get('stringyform.admin.entry_controller',
            \Smorken\StringyForm\Http\Controllers\Admin\EntryController::class);
        Route::get('/', [$controller, 'index']);
        Route::get('/export', [$controller, 'export']);
        Route::get('/view/{id}', [$controller, 'view']);
        Route::get('/delete/{id}', [$controller, 'delete']);
        Route::delete('/delete/{id}', [$controller, 'doDelete']);
    });
});
