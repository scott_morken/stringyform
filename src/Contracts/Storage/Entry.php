<?php

namespace Smorken\StringyForm\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

interface Entry extends Base
{
    public function findFirstByFormId(
        int $formId,
        int $userId,
        bool $shouldCreate = true
    ): ?\Smorken\StringyForm\Contracts\Models\Entry;

    public function setUpdatedBy(
        \Smorken\StringyForm\Contracts\Models\Entry $entry,
        int $userId
    ): \Smorken\StringyForm\Contracts\Models\Entry;
}
