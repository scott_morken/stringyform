<?php

namespace Smorken\StringyForm\Providers\Services;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\GateService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;
use Smorken\StringyForm\Contracts\Services\Entries\CreateService;
use Smorken\StringyForm\Contracts\Services\Entries\DeleteService;
use Smorken\StringyForm\Contracts\Services\Entries\EntryFactory;
use Smorken\StringyForm\Contracts\Services\Entries\IndexService;
use Smorken\StringyForm\Contracts\Services\Entries\RetrieveService;
use Smorken\StringyForm\Contracts\Services\Entries\SaveFactory;
use Smorken\StringyForm\Contracts\Services\Entries\UpdateService;
use Smorken\StringyForm\Contracts\Storage\Entry;
use Smorken\StringyForm\Contracts\Storage\EntryData;

class EntryServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, fn ($app) => new \Smorken\StringyForm\Services\Entries\IndexService(
            $app[Entry::class],
            [
                FilterService::class => new \Smorken\StringyForm\Services\Entries\FilterService,
            ]
        ));
        $this->getApp()->bind(RetrieveService::class, function ($app) {
            $gate = $app[Gate::class];
            $gate->define('Entry.view', fn ($user, \Smorken\StringyForm\Contracts\Models\Entry $entry) => (string) $user->id === (string) $entry->created_by);

            return new \Smorken\StringyForm\Services\Entries\RetrieveService(
                $app[Entry::class],
                [
                    GateService::class => new \Smorken\Service\Services\GateService($gate),
                ]
            );
        });
        $this->getApp()->bind(EntryFactory::class, function ($app) {
            $services = [
                DeleteService::class => new \Smorken\StringyForm\Services\Entries\DeleteService(
                    $app[Entry::class]
                ),
                RetrieveService::class => $app[RetrieveService::class],
                \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService::class => $app[\Smorken\StringyForm\Contracts\Services\Forms\RetrieveService::class],
                FilterService::class => new \Smorken\StringyForm\Services\Entries\FilterService,
            ];
            $additionalServices = [
                FilterService::class => new \Smorken\StringyForm\Services\Entries\FilterService,
            ];

            return new \Smorken\StringyForm\Services\Entries\EntryFactory($services, $additionalServices);
        });
        $this->getApp()->bind(SaveFactory::class, function ($app) {
            $ep = $app[Entry::class];
            $edp = $app[EntryData::class];
            $services = [
                CreateService::class => new \Smorken\StringyForm\Services\Entries\CreateService(
                    $ep,
                    $edp
                ),
                UpdateService::class => new \Smorken\StringyForm\Services\Entries\UpdateService(
                    $ep,
                    $edp
                ),
            ];
            $additionalServices = [
                \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService::class => $app[\Smorken\StringyForm\Contracts\Services\Forms\RetrieveService::class],
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return new \Smorken\StringyForm\Services\Entries\SaveFactory($services, $additionalServices);
        });
    }
}
