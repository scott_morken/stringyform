<?php

namespace Smorken\StringyForm\Storage\Eloquent;

use Smorken\StringyForm\Validation\RuleProviders\EntryDataRules;

class EntryData extends Base implements \Smorken\StringyForm\Contracts\Storage\EntryData
{
    public function updateValue(
        \Smorken\StringyForm\Contracts\Models\EntryData $element,
        $value
    ): \Smorken\StringyForm\Contracts\Models\EntryData {
        $element->value = $value;
        $element->save();

        return $element;
    }

    public function validationRules(array $override = []): array
    {
        return [...EntryDataRules::rules(), ...$override];
    }
}
