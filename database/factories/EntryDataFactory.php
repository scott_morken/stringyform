<?php

namespace Database\Factories\Smorken\StringyForm\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\StringyForm\Models\Eloquent\EntryData;

/**
 * @extends Factory<EntryData>
 */
class EntryDataFactory extends Factory
{
    protected $model = EntryData::class;

    public function definition(): array
    {
        return [
            'entry_id' => $this->faker->randomNumber(2),
            'element_id' => $this->faker->randomNumber(2),
            'value' => $this->faker->randomElement(['foo', 'bar', 1, 0]),
        ];
    }
}
