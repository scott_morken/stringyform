<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Actions;

use Illuminate\Support\Collection;
use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\Domain\Actions\Concerns\HasValidationFactory;
use Smorken\StringyForm\Contracts\Actions\Results\CreateElementsResult;
use Smorken\StringyForm\Contracts\Models\Entry;
use Smorken\StringyForm\Contracts\Models\EntryData;
use Smorken\StringyForm\VOs\Element;

/**
 * @extends ActionWithEloquent<\Smorken\StringyForm\Models\Eloquent\EntryData>
 */
class CreateElementsAction extends ActionWithEloquent implements \Smorken\StringyForm\Contracts\Actions\CreateElementsAction
{
    use HasValidationFactory;

    public function __construct(EntryData $model)
    {
        parent::__construct($model);
    }

    public function __invoke(Collection $elements, Entry $entry): CreateElementsResult
    {
        $form = $entry->form;
        $this->validate($this->getValidatableArrayOfElements($elements), $form->element_rules->toArray());
        $existingElements = $entry->elements->keyBy('element');
        $updated = $this->updateElements($elements, $existingElements);
        $created = $this->createElements($entry->id, $elements, $existingElements);
        $deleted = $this->deleteElements($elements, $existingElements);

        return new \Smorken\StringyForm\Actions\Results\CreateElementsResult(
            $entry->id,
            $created,
            $updated,
            $deleted
        );
    }

    protected function getValidatableArrayOfElements(Collection $elements): array
    {
        $validatable = [];
        /** @var Element $element */
        foreach ($elements as $element) {
            $validatable[$element->element] = $element->value;
        }

        return $validatable;
    }

    protected function createElement(int $entryId, Element $element): bool
    {
        $newModel = $this->getModelInstance()->newInstance($element->toModelArray($entryId));

        return $newModel->save();
    }

    protected function createElements(int $entryId, Collection $requestElements, Collection $existingElements): int
    {
        $created = 0;
        /** @var \Smorken\StringyForm\VOs\Element $requestElement */
        foreach ($requestElements as $requestElement) {
            $existing = $existingElements->get($requestElement->element);
            if (! $existing && $this->createElement($entryId, $requestElement)) {
                $created++;
            }
        }

        return $created;
    }

    protected function deleteElement(EntryData $entryData): bool
    {
        return $entryData->delete();
    }

    protected function deleteElements(Collection $requestElements, Collection $existingElements): int
    {
        $deleted = 0;
        /**
         * @var string $elementName
         * @var \Smorken\StringyForm\Contracts\Models\EntryData $existingElement
         */
        foreach ($existingElements as $elementName => $existingElement) {
            if (! $requestElements->has($elementName) && $this->deleteElement($existingElement)) {
                $deleted++;
            }
        }

        return $deleted;
    }

    protected function updateElement(Element $elementVO, EntryData $entryData): bool
    {
        if ($elementVO->value !== $entryData->value) {
            $entryData->value = $elementVO->value;

            return $entryData->save();
        }

        return false;
    }

    protected function updateElements(Collection $requestElements, Collection $existingElements): int
    {
        $updated = 0;
        /** @var \Smorken\StringyForm\VOs\Element $requestElement */
        foreach ($requestElements as $requestElement) {
            $existing = $existingElements->get($requestElement->element);
            if ($existing && $this->updateElement($requestElement, $existing)) {
                $updated++;
            }
        }

        return $updated;
    }
}
