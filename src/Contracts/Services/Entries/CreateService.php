<?php

namespace Smorken\StringyForm\Contracts\Services\Entries;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\StringyForm\Contracts\Models\StringyForm;
use Smorken\StringyForm\Contracts\Storage\Entry;
use Smorken\StringyForm\Contracts\Storage\EntryData;

interface CreateService extends BaseService
{
    public function create(Request $request, int $formId): CreateOrUpdateResult;

    public function getDataProvider(): EntryData;

    public function getFormRetrieveService(): \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService;

    public function getProvider(): Entry;

    public function getValidatorService(): ValidatorService;

    public function validateElementsFromRequest(Request $request, StringyForm $form): bool;
}
