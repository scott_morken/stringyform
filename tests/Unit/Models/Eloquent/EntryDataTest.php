<?php

namespace Tests\Smorken\StringyForm\Unit\Models\Eloquent;

use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Models\Eloquent\EntryData;

class EntryDataTest extends TestCase
{
    public function testArrayValue(): void
    {
        $sut = new EntryData(['value' => ['a', 'b']]);
        $this->assertEquals(['a', 'b'], $sut->value);
        $this->assertEquals('a, b', $sut->valueToString());
        $this->assertEquals('{"element_id":0,"value":["a","b"]}', (string) $sut);
    }

    public function testHasValueArray(): void
    {
        $sut = new EntryData(['value' => ['a', 'b']]);
        $this->assertTrue($sut->hasValue('a'));
        $this->assertTrue($sut->hasValue('b'));
        $this->assertFalse($sut->hasValue('c'));
    }

    public function testHasValueString(): void
    {
        $sut = new EntryData(['value' => 'foo']);
        $this->assertTrue($sut->hasValue('foo'));
        $this->assertFalse($sut->hasValue('bar'));
    }

    public function testStringValue(): void
    {
        $sut = new EntryData(['value' => 'foo']);
        $this->assertEquals('foo', $sut->value);
        $this->assertEquals('foo', $sut->valueToString());
        $this->assertEquals('{"element_id":0,"value":"foo"}', (string) $sut);
    }
}
