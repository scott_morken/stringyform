<?php

namespace Smorken\StringyForm\Services\Forms;

use Smorken\Service\Services\RetrieveByStorageProviderService;

class RetrieveService extends RetrieveByStorageProviderService implements \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService {}
