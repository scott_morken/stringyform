@php
    $name = $name ?? \Smorken\StringyForm\Tools\ElementHelper::name('input_select');
    $id = $id ?? \Smorken\StringyForm\Tools\ElementHelper::ensureId($name);
    $value = $value ?? old($name, isset($entry) ? $entry->elementValue($name, true) : null);
    $attrs = [
        'id' => $id,
        'class' => 'form-select '.($classes ?? ''),
        'name' => $name,
    ];
    $items = $items ?? [];
@endphp
<select {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!} {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($add_attrs ?? [])) !!}>
    @foreach ($items as $k => $v)
        <option value="{{ $k }}" {{ (!is_null($value) && $value == $k) ? 'selected' : '' }}>{{ $v }}</option>
    @endforeach
</select>
