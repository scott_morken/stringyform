<?php

namespace Smorken\StringyForm\Services\Entries;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\StringyForm\Contracts\Models\Entry;
use Smorken\StringyForm\Contracts\Models\EntryData;
use Smorken\StringyForm\Contracts\Models\StringyForm;
use Smorken\StringyForm\Contracts\Services\Entries\CreateOrUpdateResult;

class UpdateService extends CreateUpdateBase implements \Smorken\StringyForm\Contracts\Services\Entries\UpdateService
{
    public function update(Request $request, int $id): CreateOrUpdateResult
    {
        $entry = $this->getProvider()->findOrFail($id);
        $form = $this->getFormById($entry->form_id);

        return $this->updateByFormAndEntry($request, $form, $entry);
    }

    public function updateByFormAndEntry(Request $request, StringyForm $form, Entry $entry): CreateOrUpdateResult
    {
        $this->canUpdate($request, $entry);
        $this->updateEntry($request, $entry);
        $elements = $this->updateElements($request, $form, $entry);

        /** @var CreateOrUpdateResult */
        return $this->newVO([
            'form' => $form,
            'entry' => $entry,
            'elements' => $elements,
        ]);
    }

    public function updateByFormId(Request $request, int $formId): CreateOrUpdateResult
    {
        $form = $this->getFormById($formId);
        $entry = $this->getProvider()->findFirstByFormId($form->id, $request->user()->id);

        return $this->updateByFormAndEntry($request, $form, $entry);
    }

    protected function canUpdate(Request $request, Entry $entry): bool
    {
        $userId = (string) $request->user()->id;
        if ($userId && $userId === (string) $entry->created_by) {
            return true;
        }
        throw new AuthorizationException;
    }

    protected function handleElement(Entry $entry, string $element, $value): EntryData
    {
        $existing = $entry->elementByName($element);
        if ($existing) {
            return $this->getDataProvider()->updateValue($existing, $value);
        }

        return $this->createElement($entry, $element, $value);
    }

    protected function updateElements(Request $request, StringyForm $form, Entry $entry): Collection
    {
        $coll = new Collection;
        $elements = $this->getElements($request, $form);
        $this->getValidatorService()->validate($elements, $form->element_rules->toArray());
        foreach ($elements as $elementName => $value) {
            $element = $this->handleElement($entry, $elementName, $value ?? '');
            $coll->push($element);
        }

        return $coll;
    }

    protected function updateEntry(Request $request, Entry $entry): void
    {
        $this->getProvider()->setUpdatedBy($entry, $request->user()->id);
    }
}
