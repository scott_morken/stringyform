<?php

namespace Tests\Smorken\StringyForm\Unit\Models\Eloquent;

use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Models\Eloquent\Entry;
use Smorken\StringyForm\Models\Eloquent\EntryData;

class EntryTest extends TestCase
{
    public function testElementDoesNotExist(): void
    {
        $sut = new Entry;
        $elements = new Collection([
            new EntryData(['element' => 'test']),
        ]);
        $sut->setRelation('elements', $elements);
        $this->assertNull($sut->elementByName('testing'));
    }

    public function testElementDoesNotExistValue(): void
    {
        Container::getInstance()->bind('request', fn () => new Request);
        $sut = new Entry;
        $elements = new Collection([
            new EntryData(['element' => 'test']),
        ]);
        $sut->setRelation('elements', $elements);
        $this->assertNull($sut->elementValue('testing'));
    }

    public function testElementExists(): void
    {
        $sut = new Entry;
        $elements = new Collection([
            new EntryData(['element' => 'test']),
        ]);
        $sut->setRelation('elements', $elements);
        $this->assertSame($elements[0], $sut->elementByName('test'));
    }

    public function testElementExistsValueArray(): void
    {
        $sut = new Entry;
        $elements = new Collection([
            new EntryData(['element' => 'test', 'value' => ['a', 'b']]),
        ]);
        $sut->setRelation('elements', $elements);
        $this->assertEquals(['a', 'b'], $sut->elementValue('test'));
    }

    public function testElementExistsValueArrayAsString(): void
    {
        $sut = new Entry;
        $elements = new Collection([
            new EntryData(['element' => 'test', 'value' => ['a', 'b']]),
        ]);
        $sut->setRelation('elements', $elements);
        $this->assertEquals('a, b', $sut->elementValue('test', true));
    }

    public function testElementExistsValueString(): void
    {
        $sut = new Entry;
        $elements = new Collection([
            new EntryData(['element' => 'test', 'value' => 'testing']),
        ]);
        $sut->setRelation('elements', $elements);
        $this->assertEquals('testing', $sut->elementValue('test'));
    }
}
