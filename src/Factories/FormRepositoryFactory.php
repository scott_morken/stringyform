<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Factories;

use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\StringyForm\Contracts\Repositories\ActiveFormsRepository;
use Smorken\StringyForm\Contracts\Repositories\FindFormRepository;

class FormRepositoryFactory extends RepositoryFactory
{
    public function active(): Collection
    {
        return $this->forIterable(ActiveFormsRepository::class, 0);
    }

    public function find(int $id, bool $throw = true): mixed
    {
        return $this->forRetrieve(FindFormRepository::class, $id, $throw);
    }
}
