<?php

namespace Smorken\StringyForm\Http\Controllers\Entry;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithService\Concerns\HasFlashMessages;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\VO\RedirectActionResult;

class DeleteController extends BaseController
{
    use HasFlashMessages;

    public function deleteEntry(Request $request, int $id): View
    {
        $result = $this->entryFactory->getRetrieveService()->findById($id);

        return $this->makeView($this->getViewName('delete'))
            ->with('model', $result->model) // @phpstan-ignore property.notFound
            ->with('filter', $this->getFilter($request));
    }

    public function deleteEntryByFormId(Request $request, int $formId): View
    {
        $result = $this->entryFactory->getRetrieveService()->findByFormId($request, $formId);

        return $this->makeView($this->getViewName('delete'))
            ->with('model', $result->model)
            ->with('filter', $this->getFilter($request));
    }

    public function doDeleteEntry(Request $request, int $id): RedirectResponse
    {
        /** @var ModelResult $result */
        $result = $this->entryFactory->getDeleteService()->deleteFromRequest($request, $id);

        return $this->deleteRedirectWithResult($request, $result);
    }

    public function doDeleteEntryByFormId(Request $request, int $formId): RedirectResponse
    {
        $result = $this->entryFactory->getDeleteService()->deleteFromFormId($request, $formId);

        return $this->deleteRedirectWithResult($request, $result);
    }

    protected function deleteRedirectOnFailure(Request $request, ModelResult $result): RedirectResponse
    {
        $request->session()->flash('flash:danger', sprintf('Error deleting resource [%s].', $result->id ?? 'new'));
        if (method_exists($this, 'onDeleteFailure')) {
            $this->onDeleteFailure($request, $result);
        }

        return (new RedirectActionResult($this->actionArray('index'), $this->getParams($request)))->redirect();
    }

    protected function deleteRedirectOnSuccess(Request $request, ModelResult $result): RedirectResponse
    {
        $request->session()->flash('flash:success', sprintf('Deleted resource [%s].', $result->id));
        if (method_exists($this, 'onDeleteSuccess')) {
            $this->onDeleteSuccess($request, $result);
        }

        return (new RedirectActionResult($this->actionArray('index'), $this->getParams($request)))->redirect();
    }

    protected function deleteRedirectWithResult(Request $request, ModelResult $result): RedirectResponse
    {
        $this->addMessagesToRequest($request, $result);
        if ($result->result) {
            return $this->deleteRedirectOnSuccess($request, $result);
        }

        return $this->deleteRedirectOnFailure($request, $result);
    }
}
