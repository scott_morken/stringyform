<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Factories;

use Smorken\Domain\Actions\Contracts\Results\DeleteResult;
use Smorken\Domain\Factories\ActionFactory;
use Smorken\StringyForm\Contracts\Actions\CreateEntryByResponseIdAction;
use Smorken\StringyForm\Contracts\Actions\DeleteEntryByResponseIdAction;
use Smorken\StringyForm\Contracts\Models\Entry;

class EntryActionFactory extends ActionFactory
{
    public function createByResponseId(int $formId, int $responseId): Entry
    {
        return $this->for(CreateEntryByResponseIdAction::class, ['formId' => $formId, 'responseId' => $responseId]);
    }

    public function deleteByResponseId(int $responseId): DeleteResult
    {
        return $this->for(DeleteEntryByResponseIdAction::class, ['responseId' => $responseId]);
    }
}
