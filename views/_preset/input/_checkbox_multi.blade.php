@php
    $value = (array) ($value ?? 1);
@endphp
@foreach($value as $v)
    @include('stringyform::_preset.input._checkbox', ['value' => $v]);
@endforeach
