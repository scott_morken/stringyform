<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\StringyForm\Http\Controllers\Entry\EntryController;
use Smorken\StringyForm\Models\Eloquent\Entry;
use Smorken\StringyForm\Models\Eloquent\EntryData;
use Smorken\StringyForm\Models\Eloquent\StringyForm;
use Tests\Smorken\StringyForm\TestBenchTestCase;

class DirectViewTest extends TestBenchTestCase
{
    use DatabaseMigrations;

    public function testViewByFormIdModelNoElements(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $response = $this->actingAs($user)
            ->get('/vfd/'.$form->id);
        $response->assertSee('<div>Test: </div>', false);
    }

    public function testViewByFormIdModelNoEntryModelFound(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $response = $this->actingAs($user)
            ->get('/vfd/'.$form->id);
        $response->assertSee('<div>Test: </div>', false);
    }

    public function testViewByFormIdModelWithElement(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'foo'])->create();
        $response = $this->actingAs($user)
            ->get('/vfd/'.$form->id);
        $response->assertSee('<div>Test: foo</div>', false);
    }

    public function testViewByFormIdNoFormModelFound(): void
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->get('/vfd/1');
        $response->assertSee('Not Found');
        $response->assertStatus(404);
        $this->assertInstanceOf(ModelNotFoundException::class, $response->baseResponse->exception);
    }

    public function testViewModelNoElements(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $response = $this->actingAs($user)
            ->get('/vd/'.$entry->id);
        $response->assertSee('<div>Test: </div>', false);
    }

    public function testViewModelWithElement(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'foo'])->create();
        $response = $this->actingAs($user)
            ->get('/vd/'.$entry->id);
        $response->assertSee('<div>Test: foo</div>', false);
    }

    public function testViewModelWithElementNotAuthorized(): void
    {
        $user = User::factory()->create();
        $form = StringyForm::factory()->create();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => ($user->id + 111)])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'foo'])->create();
        $response = $this->actingAs($user)
            ->get('/vd/'.$entry->id);
        $response->assertStatus(403);
        $response->assertSee('This action is unauthorized.', false);
    }

    public function testViewNoModelFound(): void
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->get('/vd/1');
        $response->assertSee('Not Found');
        $response->assertStatus(404);
        $this->assertInstanceOf(ModelNotFoundException::class, $response->baseResponse->exception);
    }

    protected function defineWebRoutes($router)
    {
        $router->get('/vd/{id}', [EntryController::class, 'viewEntryDirect']);
        $router->get('/vfd/{formId}', [EntryController::class, 'viewEntryByFormIdDirect']);
    }
}
