<?php

namespace Smorken\StringyForm\Http\Controllers\Admin;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithService\CrudController;
use Smorken\Service\Services\VO\RedirectActionResult;
use Smorken\String2Blade\Contracts\Support\ViewFromString;
use Smorken\StringyForm\Contracts\Services\Entries\SaveFactory;
use Smorken\StringyForm\Contracts\Services\Forms\Admin\CrudServices;
use Smorken\StringyForm\Contracts\Services\Forms\Admin\IndexService;
use Smorken\StringyForm\Models\Eloquent\Entry;
use Smorken\Support\Filter;

class StringyFormController extends CrudController
{
    protected string $baseView = 'stringyform::admin.stringyform';

    public function __construct(
        protected SaveFactory $saveFactory,
        protected ViewFromString $viewFromString,
        CrudServices $crudServices,
        IndexService $indexService
    ) {
        parent::__construct($crudServices, $indexService);
    }

    public function doPreview(Request $request, int $id): RedirectResponse
    {
        $formResult = $this->crudServices->getRetrieveService()->findById($id);
        // @phpstan-ignore property.notFound
        $this->saveFactory->getCreateService()->validateElementsFromRequest($request, $formResult->model);

        return (new RedirectActionResult($this->actionArray('view'), ['id' => $id]))->redirect();
    }

    public function preview(Request $request, int $id): View
    {
        $form = $this->crudServices->getRetrieveService()->findById($id);

        return $this->makeView($this->getViewName('preview'))
            ->with('form', $form->model) // @phpstan-ignore property.notFound
            ->with('filter', new Filter)
            ->with('entry', new Entry)
            ->with('viewFromString', $this->viewFromString);
    }
}
