<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\StringyForm\Contracts\Models\Entry;

/**
 * @extends EloquentRetrieveRepository<\Smorken\StringyForm\Models\Eloquent\Entry>
 */
class FindEntryRepository extends EloquentRetrieveRepository implements \Smorken\StringyForm\Contracts\Repositories\FindEntryRepository
{
    public function __construct(Entry $model)
    {
        parent::__construct($model);
    }
}
