<?php

declare(strict_types=1);

namespace Smorken\StringyForm\VOs;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Element implements Arrayable
{
    public function __construct(
        public string $element,
        public mixed $value
    ) {}

    public static function fromRequest(Request $request, array $formElements): Collection
    {
        $elements = new Collection;
        foreach ($formElements as $element) {
            $elements->put($element, new self($element, $request->input($element)));
        }

        return $elements;
    }

    public function toArray(): array
    {
        return [
            'element' => $this->element,
            'value' => $this->value,
        ];
    }

    public function toModelArray(int $entryId): array
    {
        return [...$this->toArray(), 'entry_id' => $entryId];
    }
}
