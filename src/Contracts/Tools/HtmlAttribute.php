<?php

namespace Smorken\StringyForm\Contracts\Tools;

/**
 * @property string $key
 * @property mixed $value
 *
 * @phpstan-require-extends \Smorken\StringyForm\Tools\HtmlAttribute
 */
interface HtmlAttribute extends \Stringable
{
    public function shouldRender(): bool;
}
