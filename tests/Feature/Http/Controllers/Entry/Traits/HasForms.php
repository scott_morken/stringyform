<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits;

use Smorken\StringyForm\Contracts\Models\StringyForm;

trait HasForms
{
    protected function getComplexForm(bool $create = true): StringyForm
    {
        $rules = [
            'name' => 'required',
            'location' => null,
            'active' => 'bool',
            'fave_colors' => 'required|array|in:blue,red,green',
            'uni' => 'required|in:asu|arizona',
        ];

        return $this->getFormFromBladeString($this->getComplexFormBladeString(), $rules, $create);
    }

    protected function getComplexFormBladeString(): string
    {
        return file_get_contents(__DIR__.'/forms/complex_form.blade.php');
    }

    protected function getFormFromBladeString(string $blade, array $rules, bool $create = true): StringyForm
    {
        $f = \Smorken\StringyForm\Models\Eloquent\StringyForm::factory(['form' => $blade, 'element_rules' => $rules]);

        return $create ? $f->create() : $f->make();
    }

    protected function getSimpleForm(bool $create = true): StringyForm
    {
        $rules = [
            'test' => 'required',
            'check-test' => null,
        ];

        return $this->getFormFromBladeString($this->getSimpleFormBladeString(), $rules, $create);
    }

    protected function getSimpleFormBladeString(): string
    {
        return file_get_contents(__DIR__.'/forms/simple_form.blade.php');
    }
}
