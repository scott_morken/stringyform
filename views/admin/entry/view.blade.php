<?php
/**
 * @var \Smorken\StringyForm\Contracts\Models\Entry $model
 * @var \Smorken\StringyForm\Contracts\Models\StringyForm $form
 */
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter) ? $filter->all() : []);
$form = $model->form;
?>
@extends(\Illuminate\Support\Facades\Config::get('stringyform.layout', 'layouts.app'))
@section('content')
    @include('stringyform::_preset.controller._to_index')
    <h4>Entry #{{ $model->getKey() }}</h4>
    <div class="row">
        <div class="col-md-2 col-sm-12 font-weight-bold fw-bold">ID</div>
        <div class="col-md-10 col-sm-12">{{ $model->getKey() }}</div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-12 font-weight-bold fw-bold">Form</div>
        <div class="col-md-10 col-sm-12">{{ $model->form_id }}: {{ $model->form?->name ?? '--' }}</div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="card-title">Elements</div>
            @if ($form)
                @foreach($form->elements as $element)
                    <div class="row">
                        <div class="col-md-2 col-sm-12 font-weight-bold fw-bold">{{ $element }}</div>
                        <div class="col-md-10 col-sm-12">{{ $model->elementValue($element, true) }}</div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection
