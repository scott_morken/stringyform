<?php

namespace Smorken\StringyForm\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Smorken\Controller\View\WithService\CrudController;
use Smorken\Service\Services\VO\RedirectActionResult;
use Smorken\StringyForm\Contracts\Services\Entries\Admin\CrudServices;
use Smorken\StringyForm\Contracts\Services\Entries\Admin\ExportService;
use Smorken\StringyForm\Contracts\Services\Entries\Admin\IndexService;

class EntryController extends CrudController
{
    protected string $baseView = 'stringyform::admin.entry';

    public function __construct(
        CrudServices $crudServices,
        IndexService $indexService,
        protected ExportService $exportService
    ) {
        parent::__construct($crudServices, $indexService);
    }

    public function export(Request $request): Response|RedirectResponse
    {
        if (! $request->has('f_formId')) {
            $request->session()->flash('flash:info', 'Please select a form before exporting.');

            return (new RedirectActionResult($this->actionArray('index'), $this->getParams($request)))->redirect();
        }
        $result = $this->exportService->export($request);

        return $result->exporter->output('entries');
    }
}
