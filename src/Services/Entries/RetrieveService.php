<?php

namespace Smorken\StringyForm\Services\Entries;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\RetrieveByStorageProviderService;
use Smorken\StringyForm\Contracts\Models\Entry;

class RetrieveService extends RetrieveByStorageProviderService implements \Smorken\StringyForm\Contracts\Services\Entries\RetrieveService
{
    public function findByFormId(Request $request, int $formId): ModelResult
    {
        // @phpstan-ignore method.notFound
        $entry = $this->getProvider()->findFirstByFormId($formId, $request->user()->id);
        if (! $this->authorize($entry)) {
            throw new AuthorizationException("You are not authorized to view resource [{$entry->id}].");
        }

        /** @var ModelResult */
        return $this->newVO(['model' => $entry, 'id' => $entry->id, 'result' => (bool) $entry]);
    }

    public function getModel(): Entry
    {
        return $this->getProvider()->getModel()->newInstance();
    }
}
