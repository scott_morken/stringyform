<?php

namespace Smorken\StringyForm\Storage\Eloquent;

use Smorken\StringyForm\Validation\RuleProviders\StringyFormRules;

class StringyForm extends Base implements \Smorken\StringyForm\Contracts\Storage\StringyForm
{
    public function validationRules(array $override = []): array
    {
        return [...StringyFormRules::rules(), ...$override];
    }
}
