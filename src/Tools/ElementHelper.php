<?php

namespace Smorken\StringyForm\Tools;

class ElementHelper
{
    public static int $increment = 0;

    public static function ensureId(string $id): string
    {
        return preg_replace('/[-]{2,}/', '-',
            \Illuminate\Support\Str::slug(str_replace(['[', ']', '_'], '-', $id)));
    }

    public static function id(string $base = 'element', bool $increment = true): string
    {
        return sprintf('%s-%d', $base, $increment ? self::increment() : self::$increment);
    }

    public static function increment(int $count = 1): int
    {
        self::$increment = self::$increment + $count;

        return self::$increment;
    }

    public static function name(string $base = 'element', bool $increment = true): string
    {
        return sprintf('%s_%d', $base, $increment ? self::increment() : self::$increment);
    }
}
