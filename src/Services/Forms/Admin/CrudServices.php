<?php

namespace Smorken\StringyForm\Services\Forms\Admin;

use Smorken\Service\Services\CrudByStorageProviderServices;

class CrudServices extends CrudByStorageProviderServices implements \Smorken\StringyForm\Contracts\Services\Forms\Admin\CrudServices {}
