## String form compiler for Laravel 8

Provides the ability for Laravel to handle raw strings as forms.

Per the norm, you are responsible for what is in the template string!  User provided input is probably a terrible idea.

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

It should register itself automatically. If not, add to your `config/app.php`:

    ...
    'providers' => [
            Smorken\StringyForm\ServiceProvider::class,
    ...
