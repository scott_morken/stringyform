<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

interface FindFormRepository extends RetrieveRepository {}
