<?php

namespace Smorken\StringyForm\Services\Entries;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\StringyForm\Contracts\Models\StringyForm;

class CreateService extends CreateUpdateBase implements \Smorken\StringyForm\Contracts\Services\Entries\CreateService
{
    public function create(
        Request $request,
        int $formId
    ): \Smorken\StringyForm\Contracts\Services\Entries\CreateOrUpdateResult {
        $form = $this->getFormById($formId);
        $entry = $this->createEntry($request, $form);
        $elements = $this->createElements($request, $form, $entry);

        /** @var \Smorken\StringyForm\Contracts\Services\Entries\CreateOrUpdateResult */
        return $this->newVO([
            'form' => $form,
            'entry' => $entry,
            'elements' => $elements,
        ]);
    }

    public function validateElementsFromRequest(Request $request, StringyForm $form): bool
    {
        $elements = $this->getElements($request, $form);

        return $this->getValidatorService()->validate($elements, $form->element_rules->toArray());
    }

    protected function createElements(
        Request $request,
        StringyForm $form,
        \Smorken\StringyForm\Contracts\Models\Entry $entry
    ): Collection {
        $coll = new Collection;
        $elements = $this->getElements($request, $form);
        $this->getValidatorService()->validate($elements, $form->element_rules->toArray());
        foreach ($elements as $elementName => $value) {
            $element = $this->createElement($entry, $elementName, $value ?? '');
            $coll->push($element);
        }

        return $coll;
    }

    protected function createEntry(Request $request, StringyForm $form): \Smorken\StringyForm\Contracts\Models\Entry
    {
        $attributes = $this->getFormAttributes($request, $form);
        $this->getValidatorService()->validate($attributes, $this->getProvider()->validationRules());

        return $this->getProvider()->create($attributes);
    }

    protected function getFormAttributes(Request $request, StringyForm $form): array
    {
        $userId = $request->user()->id;

        return [
            'form_id' => $form->id,
            'created_by' => $userId,
            'updated_by' => $userId,
        ];
    }
}
