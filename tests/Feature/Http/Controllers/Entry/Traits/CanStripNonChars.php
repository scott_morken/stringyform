<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits;

trait CanStripNonChars
{
    protected function stripNonChars(string $content): string
    {
        return trim(preg_replace('/[ ]{2,}/', ' ', preg_replace('/[\r\n\t]/', ' ', $content)));
    }
}
