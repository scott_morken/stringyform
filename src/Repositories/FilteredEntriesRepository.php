<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Repositories;

use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\StringyForm\Contracts\Models\Entry;

/**
 * @extends EloquentFilteredRepository<\Smorken\StringyForm\Models\Eloquent\Entry>
 */
class FilteredEntriesRepository extends EloquentFilteredRepository implements \Smorken\StringyForm\Contracts\Repositories\FilteredEntriesRepository
{
    public function __construct(Entry $model)
    {
        parent::__construct($model);
    }
}
