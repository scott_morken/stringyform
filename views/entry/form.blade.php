<?php
/**
 * @var \Smorken\StringyForm\Contracts\Models\StringyForm $form
 * @var \Smorken\StringyForm\Contracts\Models\Entry $entry
 * @var \Smorken\Support\Contracts\Filter $filter
 * @var \Smorken\String2Blade\Contracts\Support\ViewFromString $viewFromString
 */
use Illuminate\Support\Facades\Config;

$layoutComponent = Config::get('stringyform.layout', 'layouts.app');
?>
<x-dynamic-component :component="$layoutComponent">
    <x-stringyform::entry.form :form="$form" :entry="$entry" :filter="$filter"
                               :viewFromString="$viewFromString"></x-stringyform::entry.form>
</x-dynamic-component>
