<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends \Illuminate\Database\Migrations\Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = ['stringy_forms'];
        foreach ($tables as $t) {
            Schema::dropIfExists($t);
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stringy_forms', function (Blueprint $t) {
            $t->increments('id');
            $t->string('name', 64);
            $t->mediumText('form');
            $t->mediumText('view_only')->nullable();
            $t->text('element_rules')->nullable();
            $t->timestamps();
        });

        Schema::table('entry_data', function (Blueprint $t) {
            $t->string('element');
            $t->index('element', 'ed_element_ndx');
        });
    }
};
