<?php

namespace Smorken\StringyForm\Services\Entries;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\DeleteByStorageProviderService;

class DeleteService extends DeleteByStorageProviderService implements \Smorken\StringyForm\Contracts\Services\Entries\DeleteService
{
    public function deleteFromFormId(Request $request, int $formId): ModelResult
    {
        // @phpstan-ignore method.notFound
        $model = $this->getProvider()->findFirstByFormId($formId, $request->user()->id, false);
        if (! $model) {
            /** @var ModelResult */
            return $this->newVO([
                'model' => null,
                'id' => null,
                'result' => true,
                'message' => [
                    'flash:info' => ['No model found.'],
                ],
            ]);
        }

        return $this->deleteFromModel($model);
    }
}
