<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Actions;

use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\StringyForm\Contracts\Models\Entry;

/**
 * @extends ActionWithEloquent<\Smorken\StringyForm\Models\Eloquent\Entry>
 */
class CreateEntryByResponseIdAction extends ActionWithEloquent implements \Smorken\StringyForm\Contracts\Actions\CreateEntryByResponseIdAction
{
    public function __construct(Entry $model)
    {
        parent::__construct($model);
    }

    public function __invoke(int $formId, int $responseId): Entry
    {
        $entry = $this->getModelInstance()->newQuery()->responseIdIs($responseId)->first();
        if (! $entry) {
            $entry = $this->getModelInstance()->newInstance(['form_id' => $formId, 'response_id' => $responseId]);
            $entry->save();
        }

        /** @var \Smorken\StringyForm\Models\Eloquent\Entry */
        return $entry;
    }
}
