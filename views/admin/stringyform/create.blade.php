@extends(\Illuminate\Support\Facades\Config::get('stringyform.layout', 'layouts.app'))
@include('stringyform::_preset.controller.create', ['title' => 'Form Administration', 'inputs_view' => 'stringyform::admin.stringyform._form'])
