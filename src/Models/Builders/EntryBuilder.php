<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\Eloquent;
use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\QueryStringFilter\Concerns\WithQueryStringFilter;

/**
 * @template TModel of Eloquent
 *
 * @extends Builder<TModel>
 */
class EntryBuilder extends Builder
{
    use WithQueryStringFilter;

    public function completed(): EloquentBuilder
    {
        /** @var $this */
        return $this->whereHas('elements');
    }

    public function createdByIs(int|string $id): EloquentBuilder
    {
        /** @var $this */
        return $this->where('created_by', '=', $id);
    }

    public function defaultOrder(): EloquentBuilder
    {
        /** @var $this */
        return $this->orderBy('updated_at', 'desc');
    }

    public function defaultWiths(): EloquentBuilder
    {
        /** @var $this */
        return $this->with(['form', 'elements']);
    }

    public function formIdIs(int $id): EloquentBuilder
    {
        /** @var $this */
        return $this->where('form_id', '=', $id);
    }

    public function responseIdIs(int $id): EloquentBuilder
    {
        /** @var $this */
        return $this->where('response_id', '=', $id);
    }

    protected function getFilterHandlersForFilters(): array
    {
        return [
            new FilterHandler('formId', 'formIdIs'),
            new FilterHandler('responseId', 'responseIdIs'),
            new FilterHandler('userId', 'createdByIs'),
        ];
    }
}
