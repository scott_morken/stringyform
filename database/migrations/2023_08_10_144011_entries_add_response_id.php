<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends \Illuminate\Database\Migrations\Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void {}

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (! Schema::hasColumn('entries', 'response_id')) {
            Schema::table('entries', function (Blueprint $t) {
                $t->unsignedBigInteger('response_id')->default(0);
                $t->index('response_id');
            });
        }
    }
};
