<?php

namespace Smorken\StringyForm\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

interface EntryData extends Base
{
    public function updateValue(
        \Smorken\StringyForm\Contracts\Models\EntryData $element,
        $value
    ): \Smorken\StringyForm\Contracts\Models\EntryData;
}
