<?php

declare(strict_types=1);

namespace Tests\Smorken\StringyForm\Unit\Actions;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Actions\DeleteEntryByResponseIdAction;
use Smorken\StringyForm\Models\Eloquent\Entry;

class DeleteEntryByResponseIdActionTest extends TestCase
{
    #[Test]
    public function it_should_delete_a_model_if_found(): void
    {
        $query = m::mock(Builder::class);
        $model = m::mock(new Entry)->makePartial();
        $model->expects()->newInstance()->andReturn($model);
        $model->expects()->newQuery()->andReturn($query);
        $query->expects()->responseIdIs(1)->andReturn($query);
        $newModel = m::mock((new Entry)->forceFill(['id' => 11, 'response_id' => 1]))->makePartial();
        $query->expects()->first()->andReturn($newModel);
        $newModel->expects()->delete()->andReturn(true);
        $sut = new DeleteEntryByResponseIdAction($model);
        $result = $sut(1);
        $this->assertEquals(11, $result->id);
        $this->assertTrue($result->deleted);
    }

    #[Test]
    public function it_should_not_delete_a_model_if_not_found(): void
    {
        $query = m::mock(Builder::class);
        $model = m::mock(new Entry)->makePartial();
        $model->expects()->newInstance()->andReturn($model);
        $model->expects()->newQuery()->andReturn($query);
        $query->expects()->responseIdIs(1)->andReturn($query);
        $query->expects()->first()->andReturn(null);
        $sut = new DeleteEntryByResponseIdAction($model);
        $result = $sut(1);
        $this->assertEquals(0, $result->id);
        $this->assertFalse($result->deleted);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
