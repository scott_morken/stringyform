@php
    $skip_error = $skip_error ?? false;
    $name = $name ?? \Smorken\StringyForm\Tools\ElementHelper::name();
    $id = $for ?? ($id ?? \Smorken\StringyForm\Tools\ElementHelper::ensureId($name));
    $attrs = [
        'for' => $id,
        'class' => $label_classes ?? 'form-label',
    ];
@endphp
<label {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!} {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($add_attrs ?? [])) !!}>
    {{ $title ?? $name }}
    @if ($skip_error === false && isset($errors) && $errors->has($name))
    &middot; <small class="text-danger">{{ $name }} error</small>
    @endif
</label>

