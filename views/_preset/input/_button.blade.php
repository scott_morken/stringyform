@php
    $type = (!isset($type) || !is_string($type) ? 'submit' : $type);
    $name = $name ?? \Smorken\StringyForm\Tools\ElementHelper::name('button_'.$type);
    $id = $id ?? \Smorken\StringyForm\Tools\ElementHelper::ensureId($name);
    $attrs = [
        'id' => $id,
        'type' => $type,
        'name' => $name,
        'class' => 'btn '.($classes ?? ''),
    ];
@endphp
<button {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!} {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($add_attrs ?? [])) !!}>
    {{ $title ?? 'Submit' }}
</button>
