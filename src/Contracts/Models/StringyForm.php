<?php

namespace Smorken\StringyForm\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $form
 * @property string $view_only
 * @property \Smorken\StringyForm\Contracts\VOs\Rules $element_rules
 * @property-read string[] $elements Virtual elements array
 * @property \Illuminate\Support\Collection<\Smorken\StringyForm\Contracts\Models\Entry> $entries
 *
 * @phpstan-require-extends \Smorken\StringyForm\Models\Eloquent\StringyForm
 */
interface StringyForm extends Model {}
