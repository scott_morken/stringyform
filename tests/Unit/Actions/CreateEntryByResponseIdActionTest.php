<?php

declare(strict_types=1);

namespace Tests\Smorken\StringyForm\Unit\Actions;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Actions\CreateEntryByResponseIdAction;
use Smorken\StringyForm\Models\Eloquent\Entry;

class CreateEntryByResponseIdActionTest extends TestCase
{
    #[Test]
    public function it_should_create_a_new_model_if_not_found(): void
    {
        $query = m::mock(Builder::class);
        $model = m::mock(new Entry);
        $model->allows()->newInstance()->andReturn($model);
        $model->expects()->newQuery()->andReturn($query);
        $query->expects()->responseIdIs(1)->andReturn($query);
        $query->expects()->first()->andReturn(null);
        $newModel = m::mock((new Entry)->forceFill(['id' => 11, 'response_id' => 1]))->makePartial();
        $model->expects()->newInstance(['form_id' => 2, 'response_id' => 1])->andReturn($newModel);
        $newModel->expects()->save()->andReturn(true);
        $sut = new CreateEntryByResponseIdAction($model);
        $entry = $sut(2, 1);
        $this->assertEquals(11, $entry->id);
    }

    #[Test]
    public function it_should_return_an_existing_model_if_found(): void
    {
        $query = m::mock(Builder::class);
        $model = m::mock(new Entry)->makePartial();
        $model->expects()->newInstance()->andReturn($model);
        $model->expects()->newQuery()->andReturn($query);
        $query->expects()->responseIdIs(1)->andReturn($query);
        $newModel = m::mock((new Entry)->forceFill(['id' => 11, 'response_id' => 1]))->makePartial();
        $query->expects()->first()->andReturn($newModel);
        $newModel->expects()->save()->never();
        $sut = new CreateEntryByResponseIdAction($model);
        $entry = $sut(2, 1);
        $this->assertEquals(11, $entry->id);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
