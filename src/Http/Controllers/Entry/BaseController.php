<?php

namespace Smorken\StringyForm\Http\Controllers\Entry;

use Illuminate\Http\Request;
use Smorken\Controller\View\Controller;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\StringyForm\Contracts\Services\Entries\EntryFactory;
use Smorken\Support\Contracts\Filter;

abstract class BaseController extends Controller
{
    protected string $baseView = 'stringyform::entry';

    public function __construct(protected EntryFactory $entryFactory)
    {
        parent::__construct();
    }

    protected function getFilter(Request $request): Filter
    {
        return $this->getFilterService()->getFilterFromRequest($request);
    }

    protected function getFilterService(): FilterService
    {
        return $this->entryFactory->getFilterService();
    }

    protected function getParams(Request $request): array
    {
        return $this->getFilter($request)->toArray();
    }

    protected function hasFilterService(): bool
    {
        return true;
    }
}
