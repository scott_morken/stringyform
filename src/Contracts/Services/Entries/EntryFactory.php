<?php

namespace Smorken\StringyForm\Contracts\Services\Entries;

use Smorken\Service\Contracts\Services\Factory;
use Smorken\Service\Contracts\Services\FilterService;

interface EntryFactory extends Factory
{
    public function getDeleteService(): DeleteService;

    public function getFilterService(): FilterService;

    public function getFormRetrieveService(): \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService;

    public function getRetrieveService(): RetrieveService;
}
