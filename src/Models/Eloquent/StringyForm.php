<?php

namespace Smorken\StringyForm\Models\Eloquent;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Eloquent;
use Smorken\StringyForm\Contracts\VOs\Rules;
use Smorken\StringyForm\Models\Builders\FormBuilder;

class StringyForm extends Eloquent implements \Smorken\StringyForm\Contracts\Models\StringyForm
{
    /** @use HasBuilder<FormBuilder> */
    use HasBuilder;

    /** @use HasFactory<\Database\Factories\Smorken\StringyForm\Models\Eloquent\StringyFormFactory> */
    use HasFactory;

    protected static string $builder = FormBuilder::class;

    protected $fillable = ['name', 'form', 'element_rules', 'view_only'];

    public function __toString(): string
    {
        return $this->name;
    }

    public function elementRules(): Attribute
    {
        return Attribute::make(
            get: function (Rules|array|string|null $value): Rules {
                $value ??= [];
                if (is_a($value, Rules::class)) {
                    return $value;
                }

                return new \Smorken\StringyForm\VOs\Rules($value);
            },
            set: function (Rules|array|string|null $value): Rules {
                $value ??= [];
                if (is_a($value, Rules::class)) {
                    return $value;
                }

                return new \Smorken\StringyForm\VOs\Rules($value);
            }
        );
    }

    public function elements(): Attribute
    {
        return Attribute::make(
            get: fn ($value): array => $this->element_rules->elements()
        );
    }

    public function entries(): HasMany
    {
        return $this->hasMany(Entry::class, 'form_id');
    }
}
