<?php

namespace Smorken\StringyForm\Contracts\Services\Entries;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \Smorken\StringyForm\Contracts\Models\StringyForm|null $form
 * @property \Smorken\StringyForm\Contracts\Models\Entry|null $entry
 * @property \Illuminate\Support\Collection<\Smorken\StringyForm\Contracts\Models\EntryData> $elements
 *
 * @phpstan-require-extends \Smorken\StringyForm\Services\Entries\CreateOrUpdateResult
 */
interface CreateOrUpdateResult extends VOResult {}
