@php use Smorken\Support\Filter; @endphp
@props([
    'form',
    'entry',
    'viewFromString',
    'filter' => new Filter()
])
<div class="stringy-form">
    <h4>Entry [{{ $entry->id }}] for Form [{{ $form->id }}]</h4>
    @if ($form->form)
        {{ $viewFromString->view($form->form, [
            'form' => $form,
            'entry' => $entry,
            'filter' => $filter,
        ]) }}
    @else
        <div class="alert alert-danger">Form data missing. Please contact your administrator.</div>
    @endif
</div>
