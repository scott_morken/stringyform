<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Actions;

use Illuminate\Support\MessageBag;
use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\Domain\Actions\Contracts\Results\DeleteResult;
use Smorken\StringyForm\Contracts\Models\Entry;

/**
 * @extends ActionWithEloquent<\Smorken\StringyForm\Models\Eloquent\Entry>
 */
class DeleteEntryByResponseIdAction extends ActionWithEloquent implements \Smorken\StringyForm\Contracts\Actions\DeleteEntryByResponseIdAction
{
    public function __construct(Entry $model)
    {
        parent::__construct($model);
    }

    public function __invoke(int $responseId): DeleteResult
    {
        $m = $this->getModelInstance()->newQuery()->responseIdIs($responseId)->first();
        $id = $m?->id ?? 0;
        $deleted = $m?->delete() ?? false;

        return new \Smorken\Domain\Actions\Results\DeleteResult($id, $deleted, new MessageBag);
    }
}
