<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\StringyForm\Http\Controllers\Entry\EntryController;
use Smorken\StringyForm\Http\Controllers\Entry\SaveController;
use Smorken\StringyForm\Models\Eloquent\Entry;
use Smorken\StringyForm\Models\Eloquent\EntryData;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\CanStripNonChars;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\HasForms;
use Tests\Smorken\StringyForm\TestBenchTestCase;

class UpdateTest extends TestBenchTestCase
{
    use CanStripNonChars, DatabaseMigrations, HasForms;

    public function testUpdateSimpleFormPostCanUpdateElement(): void
    {
        $user = User::factory()->create();
        $form = $this->getSimpleForm();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'blah blah blah'])
            ->create();
        $response = $this->actingAs($user)
            ->post('/u/'.$entry->id, ['test' => 'hello world', 'check-test' => '1']);
        $response->assertRedirect('/');
        $this->assertDatabaseMissing(EntryData::class, ['element' => 'test', 'value' => '"blah blah blah"']);
        $this->assertDatabaseHas(EntryData::class, ['element' => 'test', 'value' => '"hello world"']);
        $this->assertDatabaseHas(EntryData::class, ['element' => 'check-test', 'value' => '"1"']);
    }

    public function testUpdateSimpleFormPostFailsValidation(): void
    {
        $user = User::factory()->create();
        $form = $this->getSimpleForm();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'blah blah blah'])
            ->create();
        $response = $this->actingAs($user)
            ->post('/u/'.$entry->id, []);
        $expected = [
            'test' => [
                'The test field is required.',
            ],
        ];
        $this->assertEquals($expected, $response->baseResponse->exception->errors());
    }

    public function testUpdateSimpleFormPostNotAuthorized(): void
    {
        $user = User::factory()->create();
        $form = $this->getSimpleForm();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => '9999'])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'blah blah blah'])
            ->create();
        $response = $this->actingAs($user)
            ->post('/u/'.$entry->id, ['test' => 'hello world', 'check-test' => '1']);
        $response->assertStatus(403);
        $response->assertSee('This action is unauthorized');
    }

    public function testUpdateSimpleFormView(): void
    {
        $user = User::factory()->create();
        $form = $this->getSimpleForm();
        $entry = Entry::factory(['form_id' => $form->id, 'created_by' => $user->id])->create();
        $element = EntryData::factory(['entry_id' => $entry->id, 'element' => 'test', 'value' => 'blah blah blah'])
            ->create();
        $response = $this->actingAs($user)
            ->get('/u/'.$entry->id);
        $expected = '<html> <head> </head> <body> <div class="content"> <div class="stringy-form"> <h4>Entry [1] for Form [1]</h4> <form method="post"> <label for="test">Test</label> <input type="text" value="blah blah blah" name="test" id="test"/> <label for="check-test-1"> <input type="hidden" name="check-test" id="check-test-0" value="0"/> <input type="checkbox" name="check-test" id="check-test-1" value="1" /> </label> <button type="submit">Submit</button> </form> </div> </div> </body> </html>';
        $this->assertEquals($expected, $this->stripNonChars($response->getContent()));
    }

    protected function defineWebRoutes($router)
    {
        $router->get('/', [EntryController::class, 'index']);
        $router->get('/u/{id}', [EntryController::class, 'updateEntry']);
        $router->post('/u/{id}', [SaveController::class, 'doUpdateEntry']);
    }
}
