<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;

class FormBuilder extends Builder
{
    public function defaultOrder(): EloquentBuilder
    {
        /** @var $this */
        return $this->orderBy('name');
    }
}
