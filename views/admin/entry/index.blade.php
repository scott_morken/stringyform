<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \Smorken\StringyForm\Contracts\Models\Entry $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends(\Illuminate\Support\Facades\Config::get('stringyform.layout', 'layouts.app'))
@section('content')
    @include('stringyform::_preset.controller._title', ['title' => 'Entry Administration'])
    @includeIf('stringyform::admin.entry._filter_form')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Form ID</th>
                <th>Form</th>
                <th>User ID</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr id="row-for-{{ $model->getKey() }}">
                    <td>
                        @include('stringyform::_preset.controller.index_actions._view', ['value' => $model->getKey()])
                    </td>
                    <td>{{ $model->form_id }}</td>
                    <td>{{ $model->form?->name ?? '--' }}</td>
                    <td>{{ $model->created_by }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
