<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\IterableRepository;

interface ActiveFormsRepository extends IterableRepository {}
