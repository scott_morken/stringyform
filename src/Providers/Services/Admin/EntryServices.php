<?php

namespace Smorken\StringyForm\Providers\Services\Admin;

use Illuminate\Contracts\Validation\Factory;
use Smorken\Export\Contracts\Export;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;
use Smorken\StringyForm\Contracts\Services\Entries\Admin\CrudServices;
use Smorken\StringyForm\Contracts\Services\Entries\Admin\ExportService;
use Smorken\StringyForm\Contracts\Services\Entries\Admin\IndexService;
use Smorken\StringyForm\Contracts\Storage\Entry;

class EntryServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(ExportService::class, fn ($app) => new \Smorken\StringyForm\Services\Entries\Admin\ExportService(
            $app[Export::class],
            $app[Entry::class],
            [
                FilterService::class => new \Smorken\StringyForm\Services\Entries\Admin\FilterService,
            ]
        ));
        $this->getApp()->bind(IndexService::class, fn ($app) => new \Smorken\StringyForm\Services\Entries\Admin\IndexService(
            $app[Entry::class],
            [
                FilterService::class => new \Smorken\StringyForm\Services\Entries\Admin\FilterService,
            ]
        ));
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $services = [
                FilterService::class => new \Smorken\StringyForm\Services\Entries\Admin\FilterService,
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];
            $provider = $app[Entry::class];

            return \Smorken\StringyForm\Services\Entries\Admin\CrudServices::createByStorageProvider($provider,
                $services);
        });
    }
}
