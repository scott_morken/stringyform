<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\StringyForm\Contracts\Models\StringyForm;

/**
 * @extends EloquentRetrieveRepository<\Smorken\StringyForm\Models\Eloquent\StringyForm>
 */
class FindFormRepository extends EloquentRetrieveRepository implements \Smorken\StringyForm\Contracts\Repositories\FindFormRepository
{
    public function __construct(StringyForm $model)
    {
        parent::__construct($model);
    }
}
