<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Contracts\Actions\Results;

use Smorken\Domain\Actions\Contracts\Results\Result;

/**
 * @property positive-int $entryId
 * @property positive-int $created
 * @property positive-int $updated
 * @property positive-int $deleted
 *
 * @phpstan-require-extends \Smorken\StringyForm\Actions\Results\CreateElementsResult
 */
interface CreateElementsResult extends Result {}
