<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="form-inline" method="get">
            <div class="form-group mb-2 mr-2">
                @include('stringyform::_preset.input._label', ['name' => 'form_id', 'title' => 'Form', 'label_classes' => 'sr-only visually-hidden'])
                @include('stringyform::_preset.input._select', [
                'name' => 'form_id',
                'classes' => $filter->form_id ? $filtered : '',
                'value' => $filter->form_id,
                'items' => ['' => '-- Select One --'] + $forms->pluck('name', 'id')->all()
                ])
            </div>
            <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
            <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
        </form>
    </div>
</div>
