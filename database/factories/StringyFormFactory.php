<?php

namespace Database\Factories\Smorken\StringyForm\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\StringyForm\Models\Eloquent\StringyForm;

/**
 * @extends Factory<StringyForm>
 */
class StringyFormFactory extends Factory
{
    protected $model = StringyForm::class;

    public function definition(): array
    {
        return [
            'name' => 'Form '.$this->faker->randomNumber(2),
            'form' => '<form><input type="text" name="test" value="{{ $entry->elementValue(\'test\') }}"/></form>',
            'view_only' => '<div>Test: {{ $entry->elementValue(\'test\') }}</div>',
            'element_rules' => 'test=>required',
        ];
    }
}
