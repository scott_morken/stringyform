<form method="post">
    <label for="test">Test</label>
    <input type="text" value="{{ $entry->elementValue('test') }}" name="test" id="test"/>
    <label for="check-test-1">
        <input type="hidden" name="check-test" id="check-test-0" value="0"/>
        <input type="checkbox" name="check-test" id="check-test-1"
               value="1" {{ $entry->elementHasValue('check-test', '1') ? 'checked' : '' }}/>
    </label>
    <button type="submit">Submit</button>
</form>
