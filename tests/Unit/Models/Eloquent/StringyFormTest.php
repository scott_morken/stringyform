<?php

namespace Tests\Smorken\StringyForm\Unit\Models\Eloquent;

use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Contracts\VOs\Rules;
use Smorken\StringyForm\Models\Eloquent\StringyForm;

class StringyFormTest extends TestCase
{
    public function testGetEmptyRules(): void
    {
        $sut = new StringyForm;
        $this->assertInstanceOf(Rules::class, $sut->element_rules);
        $this->assertEmpty($sut->element_rules->toArray());
    }

    public function testSetRulesFromMixedInput(): void
    {
        $sut = new StringyForm([
            'element_rules' => [
                'foo' => 'required|int|min:1',
                'bar=>optional|max:64',
                'fiz' => 'int',
            ],
        ]);
        $expected = [
            'foo' => 'required|int|min:1',
            'bar' => 'optional|max:64',
            'fiz' => 'int',
        ];
        $this->assertEquals($expected, $sut->element_rules->toArray());
    }

    public function testSetRulesFromString(): void
    {
        $sut = new StringyForm([
            'element_rules' => <<<'RULES'
foo=>required|int|min:1
bar=>optional|max:64
fiz => int
buz=>
RULES
            ,
        ]);
        $expected = [
            'foo' => 'required|int|min:1',
            'bar' => 'optional|max:64',
            'fiz' => 'int',
            'buz' => null,
        ];
        $this->assertEquals($expected, $sut->element_rules->toArray());
    }
}
