<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Models\Observers;

use Illuminate\Contracts\Auth\Guard;
use Smorken\Model\Contracts\Model;

class CreatedByUpdatedByObserver
{
    public function __construct(protected Guard $guard) {}

    public function saving(Model $model): void
    {
        $userId = $this->guard->user()?->getAuthIdentifier() ?? 0;
        if (! $model->created_by) { // @phpstan-ignore property.notFound
            $model->created_by = $userId;
        }
        $model->updated_by = $userId; // @phpstan-ignore property.notFound
    }
}
