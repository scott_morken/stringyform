<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

interface FilteredEntriesRepository extends FilteredRepository {}
