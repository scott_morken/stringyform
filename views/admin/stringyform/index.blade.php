<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Training $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends(\Illuminate\Support\Facades\Config::get('stringyform.layout', 'layouts.app'))
@include('stringyform::_preset.controller.index', [
    'title' => 'Form Administration',
    'filter_form_view' => null,
    'limit_columns' => ['id', 'name', 'updated_at']
])
