<?php

namespace Tests\Smorken\StringyForm\Unit\Services\Entries;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\StringyForm\Contracts\Services\Entries\UpdateService;
use Smorken\StringyForm\Contracts\Services\Forms\RetrieveService;
use Smorken\StringyForm\Contracts\Storage\Entry;
use Smorken\StringyForm\Contracts\Storage\EntryData;
use Smorken\StringyForm\Contracts\Storage\StringyForm;
use Smorken\StringyForm\Validation\RuleProviders\EntryRules;
use Tests\Smorken\StringyForm\Concerns\WithMockConnection;

class UpdateServiceTest extends TestCase
{
    use WithMockConnection;

    public function testUpdateByFormIdNoFormModelIsException(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = $this->getSut();
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(99)
            ->andThrow(new ModelNotFoundException);
        $this->expectException(ModelNotFoundException::class);
        $sut->updateByFormId($request, 99);
    }

    public function testUpdateByFormIdPassesValidationWithMixedElements(): void
    {
        $request = new Request(['foo' => 'bar', 'fiz' => 'buz']);
        $request->setUserResolver(function () {
            return (new \Illuminate\Foundation\Auth\User)->forceFill(['id' => 2]);
        });
        $sut = $this->getSut();

        $formModel = (new \Smorken\StringyForm\Models\Eloquent\StringyForm)->forceFill([
            'id' => 99,
            'form' => '<form></form>',
            'element_rules' => [
                'foo' => 'required',
                'fiz' => 'required',
            ],
        ]);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(99)
            ->andReturn($formModel);
        $entryModel = (new \Smorken\StringyForm\Models\Eloquent\Entry)->forceFill([
            'id' => 1,
            'form_id' => 99,
            'created_by' => 2,
        ]);
        $entryModel->setRelation('elements', new Collection);
        $sut->getProvider()->shouldReceive('findFirstByFormId')
            ->once()
            ->with(99, 2)
            ->andReturn($entryModel);
        $sut->getProvider()->shouldReceive('setUpdatedBy')
            ->once()
            ->with($entryModel, 2);
        $elements = [
            (new \Smorken\StringyForm\Models\Eloquent\EntryData)->forceFill([
                'id' => 1,
                'form_id' => 99,
                'element_id' => 0,
                'element' => 'foo',
                'value' => 'bar',
            ]),
            (new \Smorken\StringyForm\Models\Eloquent\EntryData)->forceFill([
                'id' => 2,
                'form_id' => 99,
                'element_id' => 0,
                'element' => 'fiz',
                'value' => 'buz',
            ]),
        ];
        $sut->getDataProvider()->shouldReceive('create')
            ->once()
            ->with(['entry_id' => 1, 'element_id' => 0, 'element' => 'foo', 'value' => 'bar'])
            ->andReturn($elements[0]);
        $sut->getDataProvider()->shouldReceive('create')
            ->once()
            ->with(['entry_id' => 1, 'element_id' => 0, 'element' => 'fiz', 'value' => 'buz'])
            ->andReturn($elements[1]);
        $result = $sut->updateByFormId($request, 99);
        $this->assertSame($formModel, $result->form);
        $this->assertSame($entryModel, $result->entry);
        $this->assertSame($elements[0], $result->elements[0]);
        $this->assertSame($elements[1], $result->elements[1]);
    }

    public function testUpdateCanFailAuthorization(): void
    {
        $request = new Request(['foo' => 'bar']);
        $request->setUserResolver(function () {
            return (new \Illuminate\Foundation\Auth\User)->forceFill(['id' => 5]);
        });
        $sut = $this->getSut();
        $entryModel = (new \Smorken\StringyForm\Models\Eloquent\Entry)->forceFill([
            'id' => 1,
            'form_id' => 99,
            'created_by' => 2,
        ]);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($entryModel);
        $formModel = (new \Smorken\StringyForm\Models\Eloquent\StringyForm)->forceFill([
            'id' => 99,
            'form' => '<form></form>',
            'element_rules' => [
                'foo' => 'required',
                'bar' => 'required',
            ],
        ]);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(99)
            ->andReturn($formModel);
        $this->expectException(AuthorizationException::class);
        $sut->update($request, 1);
    }

    public function testUpdateFailsValidation(): void
    {
        $request = new Request(['foo' => 'bar']);
        $request->setUserResolver(function () {
            return (new \Illuminate\Foundation\Auth\User)->forceFill(['id' => 2]);
        });
        $sut = $this->getSut();
        $entryModel = (new \Smorken\StringyForm\Models\Eloquent\Entry)->forceFill([
            'id' => 1,
            'form_id' => 99,
            'created_by' => 2,
        ]);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($entryModel);
        $formModel = (new \Smorken\StringyForm\Models\Eloquent\StringyForm)->forceFill([
            'id' => 99,
            'form' => '<form></form>',
            'element_rules' => [
                'foo' => 'required',
                'bar' => 'required',
            ],
        ]);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(99)
            ->andReturn($formModel);
        $sut->getProvider()->shouldReceive('setUpdatedBy')
            ->once()
            ->with($entryModel, 2);
        $this->expectException(ValidationException::class);
        $sut->update($request, 1);
    }

    public function testUpdateNoEntryModelIsException(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andThrow(new ModelNotFoundException);
        $this->expectException(ModelNotFoundException::class);
        $sut->update($request, 1);
    }

    public function testUpdateNoFormModelIsException(): void
    {
        $request = new Request(['foo' => 'bar']);
        $sut = $this->getSut();
        $entryModel = (new \Smorken\StringyForm\Models\Eloquent\Entry)->forceFill(['id' => 1, 'form_id' => 99]);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($entryModel);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(99)
            ->andThrow(new ModelNotFoundException);
        $this->expectException(ModelNotFoundException::class);
        $sut->update($request, 1);
    }

    public function testUpdatePassesValidationWithMixedElements(): void
    {
        $request = new Request(['foo' => 'bar', 'fiz' => 'buz']);
        $request->setUserResolver(function () {
            return (new \Illuminate\Foundation\Auth\User)->forceFill(['id' => 2]);
        });
        $sut = $this->getSut();
        $elements = [
            (new \Smorken\StringyForm\Models\Eloquent\EntryData)->forceFill([
                'id' => 1,
                'form_id' => 99,
                'element_id' => 0,
                'element' => 'foo',
                'value' => 'bar',
            ]),
            (new \Smorken\StringyForm\Models\Eloquent\EntryData)->forceFill([
                'id' => 2,
                'form_id' => 99,
                'element_id' => 0,
                'element' => 'fiz',
                'value' => 'bazzzz',
            ]),
        ];
        $entryModel = (new \Smorken\StringyForm\Models\Eloquent\Entry)->forceFill([
            'id' => 1,
            'form_id' => 99,
            'created_by' => 2,
        ]);
        $entryModel->setRelation('elements', new Collection([$elements[1]]));
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($entryModel);
        $formModel = (new \Smorken\StringyForm\Models\Eloquent\StringyForm)->forceFill([
            'id' => 99,
            'form' => '<form></form>',
            'element_rules' => [
                'foo' => 'required',
                'fiz' => 'required',
            ],
        ]);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(99)
            ->andReturn($formModel);
        $sut->getProvider()->shouldReceive('setUpdatedBy')
            ->once()
            ->with($entryModel, 2);
        $sut->getDataProvider()->shouldReceive('create')
            ->once()
            ->with(['entry_id' => 1, 'element_id' => 0, 'element' => 'foo', 'value' => 'bar'])
            ->andReturn($elements[0]);
        $sut->getDataProvider()->shouldReceive('updateValue')
            ->once()
            ->with($elements[1], 'buz')
            ->andReturn($elements[1]);
        $result = $sut->update($request, 1);
        $this->assertSame($formModel, $result->form);
        $this->assertSame($entryModel, $result->entry);
        $this->assertSame($elements[0], $result->elements[0]);
        $this->assertSame($elements[1], $result->elements[1]);
    }

    public function testUpdatePassesValidationWithNoExistingElements(): void
    {
        $request = new Request(['foo' => 'bar', 'fiz' => 'buz']);
        $request->setUserResolver(function () {
            return (new \Illuminate\Foundation\Auth\User)->forceFill(['id' => 2]);
        });
        $sut = $this->getSut();
        $entryModel = (new \Smorken\StringyForm\Models\Eloquent\Entry)->forceFill([
            'id' => 1,
            'form_id' => 99,
            'created_by' => 2,
        ]);
        $entryModel->setRelation('elements', new Collection);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($entryModel);
        $formModel = (new \Smorken\StringyForm\Models\Eloquent\StringyForm)->forceFill([
            'id' => 99,
            'form' => '<form></form>',
            'element_rules' => [
                'foo' => 'required',
                'fiz' => 'required',
            ],
        ]);
        $sut->getFormRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(99)
            ->andReturn($formModel);
        $sut->getProvider()->shouldReceive('setUpdatedBy')
            ->once()
            ->with($entryModel, 2);
        $elements = [
            (new \Smorken\StringyForm\Models\Eloquent\EntryData)->forceFill([
                'id' => 1,
                'form_id' => 99,
                'element_id' => 0,
                'element' => 'foo',
                'value' => 'bar',
            ]),
            (new \Smorken\StringyForm\Models\Eloquent\EntryData)->forceFill([
                'id' => 2,
                'form_id' => 99,
                'element_id' => 0,
                'element' => 'fiz',
                'value' => 'buz',
            ]),
        ];
        $sut->getDataProvider()->shouldReceive('create')
            ->once()
            ->with(['entry_id' => 1, 'element_id' => 0, 'element' => 'foo', 'value' => 'bar'])
            ->andReturn($elements[0]);
        $sut->getDataProvider()->shouldReceive('create')
            ->once()
            ->with(['entry_id' => 1, 'element_id' => 0, 'element' => 'fiz', 'value' => 'buz'])
            ->andReturn($elements[1]);
        $result = $sut->update($request, 1);
        $this->assertSame($formModel, $result->form);
        $this->assertSame($entryModel, $result->entry);
        $this->assertSame($elements[0], $result->elements[0]);
        $this->assertSame($elements[1], $result->elements[1]);
    }

    protected function getSut(): UpdateService
    {
        $sfp = m::mock(StringyForm::class);
        $sfp->shouldReceive('getModel')->andReturn(new \Smorken\StringyForm\Models\Eloquent\StringyForm);
        $services = [
            RetrieveService::class => new \Smorken\StringyForm\Services\Forms\RetrieveService(
                $sfp
            ),
            ValidatorService::class => new \Smorken\Service\Services\ValidatorService(new Factory(new Translator(new ArrayLoader,
                'en'))),
        ];
        $ep = m::mock(Entry::class);
        $ep->shouldReceive('validationRules')
            ->andReturn(EntryRules::rules());

        return new \Smorken\StringyForm\Services\Entries\UpdateService(
            $ep,
            m::mock(EntryData::class),
            $services
        );
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
