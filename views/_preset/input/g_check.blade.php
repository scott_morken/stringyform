@php
$name = $name ?? \Smorken\StringyForm\Tools\ElementHelper::name('input_checkbox');
$id = ($id ?? \Smorken\StringyForm\Tools\ElementHelper::ensureId($name)).'-'.($value ?? 1);
@endphp
<div {!! (new \Smorken\StringyForm\Tools\HtmlAttributes(['class' => 'form-check '.($wrapper_classes??'')])) !!}>
    @include('stringyform::_preset.input._checkbox')
    @include('stringyform::_preset.input._label', ['id' => $id, 'label_classes' => 'form-check-label '.($label_classes??'')])
</div>
