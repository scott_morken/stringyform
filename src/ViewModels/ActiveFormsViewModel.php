<?php

declare(strict_types=1);

namespace Smorken\StringyForm\ViewModels;

use Smorken\Domain\ViewModels\IterableViewModel;

class ActiveFormsViewModel extends IterableViewModel
{
    public function toSelectList(): array
    {
        // @phpstan-ignore method.nonObject
        return $this->models()->pluck('name', 'id')->toArray();
    }
}
