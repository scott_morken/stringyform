@php
    $name = $name ?? \Smorken\StringyForm\Tools\ElementHelper::name('textarea');
    $id = $id ?? \Smorken\StringyForm\Tools\ElementHelper::ensureId($name);
    $value = $value ?? old($name, isset($entry) ? $entry->elementValue($name, true) : null);
    $attrs = [
        'id' => $id,
        'name' => $name,
        'class' => 'form-control '.($classes ?? ''),
        'placeholder' => $placeholder ?? false,
        'maxlength' => $maxlength ?? 2048,
        'rows' => $rows ?? 3
    ];
@endphp
<textarea {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!} {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($add_attrs ?? [])) !!}>{{ $value }}</textarea>
