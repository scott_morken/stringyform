<?php

namespace Smorken\StringyForm\Http\Controllers\Entry;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Controller\Controller;
use Smorken\Service\Services\VO\RedirectActionResult;
use Smorken\StringyForm\Contracts\Services\Entries\CreateOrUpdateResult;
use Smorken\StringyForm\Contracts\Services\Entries\SaveFactory;

class SaveController extends Controller
{
    public static array $formRedirectAction = [EntryController::class, 'index'];

    public function __construct(protected SaveFactory $saveFactory)
    {
        parent::__construct();
    }

    public function doCreateEntry(Request $request, int $formId): RedirectResponse
    {
        $result = $this->saveFactory->getCreateService()->create($request, $formId);
        $this->messagesFromResult($request, $result, 'create');

        return (new RedirectActionResult(self::$formRedirectAction))->redirect();
    }

    public function doUpdateEntry(Request $request, int $id): RedirectResponse
    {
        $result = $this->saveFactory->getUpdateService()->update($request, $id);
        $this->messagesFromResult($request, $result, 'update');

        return (new RedirectActionResult(self::$formRedirectAction))->redirect();
    }

    public function doUpdateEntryByFormId(Request $request, int $formId): RedirectResponse
    {
        $result = $this->saveFactory->getUpdateService()->updateByFormId($request, $formId);
        $this->messagesFromResult($request, $result, 'update');

        return (new RedirectActionResult(self::$formRedirectAction))->redirect();
    }

    protected function messagesFromResult(Request $request, CreateOrUpdateResult $result, string $type = 'create'): void
    {
        if ($result->form && $result->entry) {
            $request->session()->flash('flash:success', sprintf(
                'Saved %d elements for form [%d], entry [%d].',
                $result->elements->count(),
                $result->form->id,
                $result->entry->id
            ));

            return;
        }
        $request->session()->flash('flash:danger', 'There was an error saving your entry.');
    }
}
