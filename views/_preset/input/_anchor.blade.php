@php
    $id = $id ?? \Smorken\StringyForm\Tools\ElementHelper::id('anchor', true);
    $attrs = [
        'id' => $id,
        'href' => $href ?? '#',
        'title' => $title_attr ?? ($title ?? ''),
        'class' => $classes ?? '',
    ];
@endphp
<a {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!} {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($add_attrs ?? [])) !!}>
    {{ $title ?? ($href ?? 'link') }}
</a>
