<?php

declare(strict_types=1);

namespace Tests\Smorken\StringyForm\Unit\Tools;

use Illuminate\Contracts\Auth\Guard;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Tools\AuthorizeUserForEntry;

class AuthorizeUserForEntryTest extends TestCase
{
    #[Test]
    public function it_should_resolve_guard(): void
    {
        $guard = m::mock(Guard::class);
        AuthorizeUserForEntry::setGuardResolver(fn () => $guard);
        $this->assertSame($guard, AuthorizeUserForEntry::getGuard());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
