<?php

namespace Smorken\StringyForm\Contracts\Tools;

interface HtmlAttributes extends \Stringable
{
    public function add(HtmlAttribute $attribute): static;

    public function addKeyValue(string $key, mixed $value): static;

    public function addMany(array $attributes): static;

    /**
     * @return \Smorken\StringyForm\Contracts\Tools\HtmlAttribute[]
     */
    public function all(array $additional = []): array;

    public function append(HtmlAttribute $attribute): static;

    public function get(string $key): ?HtmlAttribute;

    public function indexOf(string $key): ?int;
}
