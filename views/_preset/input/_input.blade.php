@php
    $type = (!isset($type) || !is_string($type) ? 'text' : $type);
    $name = $name ?? \Smorken\StringyForm\Tools\ElementHelper::name('input_'.$type);
    $id = $id ?? \Smorken\StringyForm\Tools\ElementHelper::ensureId($name);
    $attrs = [
        'id' => $id,
        'type' => $type,
        'class' => 'form-control '.($classes ?? ''),
        'name' => $name,
        'value' => $value ?? old($name, isset($entry) ? $entry->elementValue($name, true) : ''),
        'placeholder' => $placeholder ?? false,
        'maxlength' => $maxlength ?? 255,
    ];
@endphp
<input {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!} {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($add_attrs ?? [])) !!}/>
