<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Factories;

use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\StringyForm\Contracts\Models\Entry;
use Smorken\StringyForm\Contracts\Repositories\FilteredEntriesRepository;
use Smorken\StringyForm\Contracts\Repositories\FindEntryRepository;
use Smorken\Support\Contracts\Filter;

class EntryRepositoryFactory extends RepositoryFactory
{
    public function filtered(
        QueryStringFilter|Filter $filter,
        int $perPage = 20
    ): \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Support\Collection|iterable {
        return $this->forFiltered(FilteredEntriesRepository::class, $filter, $perPage);
    }

    public function find(int $id, bool $throw = true): ?Entry
    {
        return $this->forRetrieve(FindEntryRepository::class, $id, $throw);
    }
}
