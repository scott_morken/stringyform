<?php

namespace Smorken\StringyForm\Services\Entries;

use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Services\Factory;
use Smorken\StringyForm\Contracts\Services\Entries\DeleteService;
use Smorken\StringyForm\Contracts\Services\Entries\RetrieveService;

class EntryFactory extends Factory implements \Smorken\StringyForm\Contracts\Services\Entries\EntryFactory
{
    protected array $services = [
        \Smorken\Service\Contracts\Services\FilterService::class => null,
        DeleteService::class => null,
        RetrieveService::class => null,
        \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService::class => null,
    ];

    public function getDeleteService(): DeleteService
    {
        /** @var DeleteService */
        return $this->getService(DeleteService::class);
    }

    public function getFilterService(): FilterService
    {
        /** @var FilterService */
        return $this->getService(FilterService::class);
    }

    public function getFormRetrieveService(): \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService
    {
        /** @var \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService */
        return $this->getService(\Smorken\StringyForm\Contracts\Services\Forms\RetrieveService::class);
    }

    public function getRetrieveService(): RetrieveService
    {
        /** @var RetrieveService */
        return $this->getService(RetrieveService::class);
    }
}
