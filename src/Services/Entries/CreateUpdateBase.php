<?php

namespace Smorken\StringyForm\Services\Entries;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Services\BaseService;
use Smorken\StringyForm\Contracts\Models\StringyForm;
use Smorken\StringyForm\Contracts\Storage\Entry;
use Smorken\StringyForm\Contracts\Storage\EntryData;

abstract class CreateUpdateBase extends BaseService
{
    protected string $voClass = CreateOrUpdateResult::class;

    public function __construct(protected Entry $provider, protected EntryData $dataProvider, array $services = [])
    {
        parent::__construct($services);
    }

    public function getDataProvider(): EntryData
    {
        return $this->dataProvider;
    }

    public function getFormRetrieveService(): \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService
    {
        /** @var \Smorken\StringyForm\Contracts\Services\Forms\RetrieveService */
        return $this->getService(\Smorken\StringyForm\Contracts\Services\Forms\RetrieveService::class);
    }

    public function getProvider(): Entry
    {
        return $this->provider;
    }

    public function getValidatorService(): ValidatorService
    {
        /** @var ValidatorService */
        return $this->getService(ValidatorService::class);
    }

    protected function createElement(
        \Smorken\StringyForm\Contracts\Models\Entry $entry,
        string $elementName,
        string|array $value
    ): \Smorken\StringyForm\Contracts\Models\EntryData {
        $attributes = [
            'entry_id' => $entry->id,
            'element_id' => 0,
            'element' => $elementName,
            'value' => $value,
        ];

        return $this->getDataProvider()->create($attributes);
    }

    protected function getElements(Request $request, StringyForm $form): array
    {
        $elements = [];
        foreach ($form->elements as $element) {
            $elements[$element] = $request->input($element);
        }

        return $elements;
    }

    protected function getFormById(int $id): StringyForm
    {
        /** @var \Smorken\Service\Contracts\Services\VO\ModelResult $result */
        $result = $this->getFormRetrieveService()->findById($id);

        /** @var StringyForm */
        return $result->model;
    }
}
