<?php

namespace Smorken\StringyForm\Contracts\Services\Entries;

use Smorken\Service\Contracts\Services\Factory;

interface SaveFactory extends Factory
{
    public function getCreateService(): CreateService;

    public function getUpdateService(): UpdateService;
}
