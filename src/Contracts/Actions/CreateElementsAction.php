<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Contracts\Actions;

use Illuminate\Support\Collection;
use Smorken\Domain\Actions\Contracts\Action;
use Smorken\StringyForm\Contracts\Actions\Results\CreateElementsResult;
use Smorken\StringyForm\Contracts\Models\Entry;

interface CreateElementsAction extends Action
{
    public function __invoke(Collection $elements, Entry $entry): CreateElementsResult;
}
