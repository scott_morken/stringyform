<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Validation\RuleProviders;

class StringyFormRules
{
    public static function rules(): array
    {
        return [
            'name' => 'required|max:64',
            'form' => 'required',
            'element_rules' => 'required',
        ];
    }
}
