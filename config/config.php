<?php

return [
    'layout' => 'layouts.app',
    'admin' => [
        'prefix' => 'admin',
        'middleware' => ['web', 'auth', 'can:role-admin'],
        'form_controller' => \Smorken\StringyForm\Http\Controllers\Admin\StringyFormController::class,
        'entry_controller' => \Smorken\StringyForm\Http\Controllers\Admin\EntryController::class,
    ],
    'providers' => [
        \Smorken\StringyForm\Contracts\Storage\Entry::class => [
            'impl' => \Smorken\StringyForm\Storage\Eloquent\Entry::class,
            'model' => \Smorken\StringyForm\Models\Eloquent\Entry::class,
        ],
        \Smorken\StringyForm\Contracts\Storage\EntryData::class => [
            'impl' => \Smorken\StringyForm\Storage\Eloquent\EntryData::class,
            'model' => \Smorken\StringyForm\Models\Eloquent\EntryData::class,
        ],
        \Smorken\StringyForm\Contracts\Storage\StringyForm::class => [
            'impl' => \Smorken\StringyForm\Storage\Eloquent\StringyForm::class,
            'model' => \Smorken\StringyForm\Models\Eloquent\StringyForm::class,
        ],
    ],
    'actions' => [
        \Smorken\StringyForm\Contracts\Actions\CreateElementsAction::class => \Smorken\StringyForm\Actions\CreateElementsAction::class,
        \Smorken\StringyForm\Contracts\Actions\CreateEntryByResponseIdAction::class => \Smorken\StringyForm\Actions\CreateEntryByResponseIdAction::class,
        \Smorken\StringyForm\Contracts\Actions\DeleteEntryByResponseIdAction::class => \Smorken\StringyForm\Actions\DeleteEntryByResponseIdAction::class,
    ],
    'repositories' => [
        \Smorken\StringyForm\Contracts\Repositories\ActiveFormsRepository::class => \Smorken\StringyForm\Repositories\ActiveFormsRepository::class,
        \Smorken\StringyForm\Contracts\Repositories\FilteredEntriesRepository::class => \Smorken\StringyForm\Repositories\FilteredEntriesRepository::class,
        \Smorken\StringyForm\Contracts\Repositories\FindEntryRepository::class => \Smorken\StringyForm\Repositories\FindEntryRepository::class,
        \Smorken\StringyForm\Contracts\Repositories\FindFormRepository::class => \Smorken\StringyForm\Repositories\FindFormRepository::class,
    ],
    'models' => [
        \Smorken\StringyForm\Contracts\Models\Entry::class => \Smorken\StringyForm\Models\Eloquent\Entry::class,
        \Smorken\StringyForm\Contracts\Models\EntryData::class => \Smorken\StringyForm\Models\Eloquent\EntryData::class,
        \Smorken\StringyForm\Contracts\Models\StringyForm::class => \Smorken\StringyForm\Models\Eloquent\StringyForm::class,
    ],
    'policies' => [
    ],
];
