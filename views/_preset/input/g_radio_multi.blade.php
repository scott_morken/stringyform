@php
    $value = (array) ($value ?? 1);
@endphp
<fieldset>
    @if (isset($title))
        <legend>{{ $title }}</legend>
    @endif
    @foreach ($value as $k => $v)
        @include('stringyform::_preset.input.g_radio', ['value' => $v, 'title' => is_string($k) ? $k : $v])
    @endforeach
</fieldset>
