<?php

namespace Smorken\StringyForm\Providers\Services\Admin;

use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;
use Smorken\StringyForm\Contracts\Services\Forms\Admin\IndexService;
use Smorken\StringyForm\Contracts\Storage\StringyForm;
use Smorken\StringyForm\Services\Forms\Admin\CrudServices;

class StringyFormServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, fn ($app) => new \Smorken\StringyForm\Services\Forms\Admin\IndexService(
            $app[StringyForm::class],
            [
                FilterService::class => new \Smorken\StringyForm\Services\Forms\Admin\FilterService,
            ]
        ));
        $this->getApp()->bind(\Smorken\StringyForm\Contracts\Services\Forms\Admin\CrudServices::class, function ($app) {
            $provider = $app[StringyForm::class];
            $services = [
                FilterService::class => new \Smorken\StringyForm\Services\Forms\Admin\FilterService,
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return CrudServices::createByStorageProvider($provider, $services);
        });
    }
}
