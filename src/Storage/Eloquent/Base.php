<?php

namespace Smorken\StringyForm\Storage\Eloquent;

use Smorken\Storage\Eloquent;

abstract class Base extends Eloquent {}
