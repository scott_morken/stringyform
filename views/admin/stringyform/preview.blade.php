<?php
/**
 * @var \Smorken\StringyForm\Contracts\Models\StringyForm $form
 * @var \Smorken\StringyForm\Contracts\Models\Entry $entry
 * @var \Smorken\Support\Contracts\Filter $filter
 * @var \Smorken\String2Blade\Contracts\Support\ViewFromString $viewFromString
 */
?>
@extends(\Illuminate\Support\Facades\Config::get('stringyform.layout', 'layouts.app'))
@section('content')
    @include('stringyform::_preset.input._anchor', [
        'href' => action([$controller, $action ?? 'view'], ['id' => $form->id] + (isset($filter)?$filter->all():[])),
        'title' => $title ?? 'Back',
        'classes' => 'float-end'
    ])
    <h2>Form #{{ $form->id }}</h2>
    @if ($form->form)
        {{ $viewFromString->view($form->form, [
            'form' => $form,
            'entry' => $entry,
            'filter' => $filter,
        ]) }}
    @else
        <div class="alert alert-danger">Form data missing. Please contact your administrator.</div>
    @endif
@endsection
