<?php

namespace Smorken\StringyForm\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * @property int $id
 * @property int $entry_id
 * @property int $element_id - always zero in stringyform (exists for bc)
 * @property string $element
 * @property array|string $value
 * @property \Smorken\StringyForm\Contracts\Models\Entry $entry
 *
 * @phpstan-require-extends \Smorken\StringyForm\Models\Eloquent\EntryData
 */
interface EntryData extends Model
{
    public function hasValue(string $value): bool;

    public function valueToString(): ?string;
}
