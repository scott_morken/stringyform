<?php

namespace Smorken\StringyForm\Services\Entries\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \Smorken\StringyForm\Contracts\Services\Entries\Admin\IndexService {}
