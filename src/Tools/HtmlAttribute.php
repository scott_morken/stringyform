<?php

namespace Smorken\StringyForm\Tools;

class HtmlAttribute implements \Smorken\StringyForm\Contracts\Tools\HtmlAttribute
{
    public string $key;

    public mixed $value;

    public function __construct(string $key, mixed $value)
    {
        $this->key = e($key);
        $this->value = is_string($value) ? e(trim($value)) : $value;
    }

    public function __toString(): string
    {
        $format = '%s="%s"';
        if (! $this->shouldRender()) {
            return '';
        }
        if ($this->value === true) {
            return sprintf($format, $this->key, $this->key);
        }
        if (is_null($this->value)) {
            return $this->key;
        }

        return sprintf($format, $this->key, $this->value);
    }

    public function shouldRender(): bool
    {
        return $this->value !== false;
    }
}
