<?php

namespace Smorken\StringyForm\Services\Entries;

use Illuminate\Support\Collection;
use Smorken\Service\Services\VO\VOResult;
use Smorken\StringyForm\Contracts\Models\Entry;
use Smorken\StringyForm\Contracts\Models\StringyForm;

class CreateOrUpdateResult extends VOResult implements \Smorken\StringyForm\Contracts\Services\Entries\CreateOrUpdateResult
{
    public function __construct(
        public ?StringyForm $form,
        public ?Entry $entry,
        public Collection $elements
    ) {}
}
