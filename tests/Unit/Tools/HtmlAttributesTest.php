<?php

namespace Tests\Smorken\StringyForm\Unit\Tools;

use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Tools\HtmlAttribute;
use Smorken\StringyForm\Tools\HtmlAttributes;

class HtmlAttributesTest extends TestCase
{
    public function testMultiAttributes(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo'),
            new HtmlAttribute('class', 'buz biz'),
            new HtmlAttribute('fiz', 'buz'),
        ]);
        $this->assertEquals('test="foo" class="buz biz" fiz="buz"', (string) $sut);
    }

    public function testMultiAttributesCanAppend(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo'),
            new HtmlAttribute('class', 'buz biz'),
            new HtmlAttribute('fiz', 'buz'),
        ]);
        $sut->add(new HtmlAttribute('class', 'bar'));
        $this->assertEquals('test="foo" class="buz biz bar" fiz="buz"', (string) $sut);
    }

    public function testMultiAttributesCanOverwriteExisting(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo'),
            new HtmlAttribute('class', 'buz biz'),
            new HtmlAttribute('fiz', 'buz'),
        ]);
        $sut->add(new HtmlAttribute('test', 'bar'));
        $this->assertEquals('test="bar" class="buz biz" fiz="buz"', (string) $sut);
    }

    public function testMultiAttributesMixed(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo'),
            new HtmlAttribute('class', 'buz biz'),
            new HtmlAttribute('fiz', 'buz'),
            'bar' => 'baz',
        ]);
        $this->assertEquals('test="foo" class="buz biz" fiz="buz" bar="baz"', (string) $sut);
    }

    public function testMultiAttributesMixedCanAppend(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo'),
            new HtmlAttribute('class', 'buz biz'),
            new HtmlAttribute('fiz', 'buz'),
        ]);
        $sut->add(new HtmlAttribute('class', 'bar'));
        $sut->addKeyValue('class', 'baz');
        $this->assertEquals('test="foo" class="buz biz bar baz" fiz="buz"', (string) $sut);
    }

    public function testMultiAttributesMixedCanOverwriteExisting(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo'),
            new HtmlAttribute('class', 'buz biz'),
            new HtmlAttribute('fiz', 'buz'),
        ]);
        $sut->addKeyValue('test', 'bar');
        $this->assertEquals('test="bar" class="buz biz" fiz="buz"', (string) $sut);
    }

    public function testNoAttributesIsEmpty(): void
    {
        $sut = new HtmlAttributes;
        $this->assertEquals('', (string) $sut);
    }

    public function testOneAddCanAppend(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('class', 'foo bar'),
        ]);
        $sut->add(new HtmlAttribute('class', 'biz'));
        $this->assertEquals('class="foo bar biz"', (string) $sut);
    }

    public function testOneAddCanOverwriteExisting(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo'),
        ]);
        $sut->add(new HtmlAttribute('test', 'bar'));
        $this->assertEquals('test="bar"', (string) $sut);
    }

    public function testOneAppendNewWillAdd(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo bar'),
        ]);
        $sut->append(new HtmlAttribute('fiz', 'biz'));
        $this->assertEquals('test="foo bar" fiz="biz"', (string) $sut);
    }

    public function testOneAppendWillAppend(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo bar'),
        ]);
        $sut->append(new HtmlAttribute('test', 'biz'));
        $this->assertEquals('test="foo bar biz"', (string) $sut);
    }

    public function testOneAttribute(): void
    {
        $sut = new HtmlAttributes([
            new HtmlAttribute('test', 'foo'),
        ]);
        $this->assertEquals('test="foo"', (string) $sut);
    }
}
