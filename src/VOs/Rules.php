<?php

namespace Smorken\StringyForm\VOs;

class Rules implements \Smorken\StringyForm\Contracts\VOs\Rules
{
    protected array $rules = [];

    public function __construct(array|string|null $input = null)
    {
        if (! is_null($input)) {
            $this->rules = $this->convertInput($input);
        }
    }

    public static function fromInput(array|string $input): static
    {
        return new static($input);
    }

    public function __toString(): string
    {
        return (string) json_encode($this->toArray());
    }

    public function elements(): array
    {
        return array_keys($this->toArray());
    }

    public function forInput(): string
    {
        $parts = [];
        foreach ($this->toArray() as $key => $rule) {
            $parts[] = sprintf('%s=>%s', $key, $rule);
        }

        return implode(PHP_EOL, $parts);
    }

    public function toArray(): array
    {
        return $this->rules;
    }

    protected function cleanArray(array $input): array
    {
        return array_filter(array_map('trim', $input));
    }

    protected function convertInput(array|string $input): array
    {
        if (is_string($input)) {
            if ($this->isJsonString($input)) {
                return json_decode($input, true);
            }

            return $this->convertStringInputToArray($input);
        }

        return $this->getRulesFromLines($input);
    }

    protected function convertStringInputToArray(string $input): array
    {
        $lines = $this->getLinesFromStringInput($input);

        return $this->getRulesFromLines($lines);
    }

    protected function getLinesFromStringInput(string $input): array
    {
        return $this->cleanArray(explode(PHP_EOL, $input));
    }

    protected function getRuleFromLineOrRule(string $line, string|int $key): array
    {
        if (! is_int($key)) {
            return [$key, $line];
        }

        return $this->cleanArray(explode('=>', $line));
    }

    protected function getRulesFromLines(array $lines): array
    {
        $rules = [];
        foreach ($lines as $index => $line) {
            $ruleArray = $this->getRuleFromLineOrRule($line ?? '', $index);
            $key = $ruleArray[0] ?? null;
            $value = $ruleArray[1] ?? null;
            if ($key) {
                $rules[$key] = $value;
            }
        }

        return $rules;
    }

    protected function isJsonString(string $input): bool
    {
        return str_starts_with($input, '{');
    }
}
