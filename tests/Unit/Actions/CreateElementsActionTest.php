<?php

declare(strict_types=1);

namespace Tests\Smorken\StringyForm\Unit\Actions;

use Illuminate\Support\Collection;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\StringyForm\Actions\CreateElementsAction;
use Smorken\StringyForm\Models\Eloquent\Entry;
use Smorken\StringyForm\Models\Eloquent\EntryData;
use Smorken\StringyForm\Models\Eloquent\StringyForm;
use Smorken\StringyForm\VOs\Element;

class CreateElementsActionTest extends TestCase
{
    #[Test]
    public function it_can_create_one_element(): void
    {
        $model = m::mock(new EntryData);
        $model->allows()->newInstance()->andReturn($model);
        $elements = new Collection([
            new Element('foo', 'bar'),
        ]);
        $entry = (new Entry)->forceFill(['id' => 1]);
        $form = new StringyForm([
            'element_rules' => ['foo' => 'required'],
        ]);
        $entry->setRelation('form', $form);
        $entry->setRelation('elements', new Collection);
        $newModel = m::mock(new EntryData(['element' => 'foo', 'value' => 'bar', 'entry_id' => 1]));
        $model->expects()->newInstance(['element' => 'foo', 'value' => 'bar', 'entry_id' => 1])
            ->andReturn($newModel);
        $newModel->expects()->save()->andReturn(true);
        $sut = new CreateElementsAction($model);
        $result = $sut($elements, $entry);
        $this->assertEquals(1, $result->created);
        $this->assertEquals(0, $result->updated);
        $this->assertEquals(0, $result->deleted);
    }

    #[Test]
    public function it_can_delete_one_element(): void
    {
        $model = m::mock(new EntryData);
        $model->allows()->newInstance()->andReturn($model);
        $elements = new Collection;
        $entry = (new Entry)->forceFill(['id' => 1]);
        $form = new StringyForm([
            'element_rules' => ['foo' => ''],
        ]);
        $entry->setRelation('form', $form);
        $existingModel = m::mock(new EntryData(['element' => 'foo', 'value' => 'bar', 'entry_id' => 1]));
        $entry->setRelation('elements', new Collection([
            $existingModel,
        ]));

        $existingModel->expects()->delete()->andReturn(true);
        $sut = new CreateElementsAction($model);
        $result = $sut($elements, $entry);
        $this->assertEquals(0, $result->created);
        $this->assertEquals(0, $result->updated);
        $this->assertEquals(1, $result->deleted);
    }

    #[Test]
    public function it_can_fail_validation(): void
    {
        $model = m::mock(new EntryData);
        $model->allows()->newInstance()->andReturn($model);
        $elements = new Collection([
            new Element('foo', 'bar'),
        ]);
        $entry = new Entry;
        $form = new StringyForm([
            'element_rules' => ['foo' => 'required', 'fizz' => 'required'],
        ]);
        $entry->setRelation('form', $form);
        $sut = new CreateElementsAction($model);
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('validation.required');
        $sut($elements, $entry);
    }

    #[Test]
    public function it_can_update_one_element(): void
    {
        $model = m::mock(new EntryData);
        $model->allows()->newInstance()->andReturn($model);
        $elements = new Collection([
            'foo' => new Element('foo', 'bizz'),
        ]);
        $entry = (new Entry)->forceFill(['id' => 1]);
        $form = new StringyForm([
            'element_rules' => ['foo' => 'required'],
        ]);
        $entry->setRelation('form', $form);
        $existingModel = m::mock(new EntryData(['element' => 'foo', 'value' => 'bar', 'entry_id' => 1]));
        $entry->setRelation('elements', new Collection([
            $existingModel,
        ]));

        $existingModel->expects()->save()->andReturn(true);
        $sut = new CreateElementsAction($model);
        $result = $sut($elements, $entry);
        $this->assertEquals(0, $result->created);
        $this->assertEquals(1, $result->updated);
        $this->assertEquals(0, $result->deleted);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $validator = new Factory(new Translator(new ArrayLoader, 'en'));
        CreateElementsAction::setValidationFactory($validator);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
