<?php

namespace Tests\Smorken\StringyForm;

use Illuminate\Cache\CacheManager;
use Orchestra\Testbench\TestCase;
use Smorken\CacheAssist\CacheAssist;
use Smorken\Domain\ActionServiceProvider;
use Smorken\Domain\DomainServiceProvider;
use Smorken\Domain\ModelServiceProvider;
use Smorken\Domain\PolicyServiceProvider;
use Smorken\Domain\RepositoryServiceProvider;
use Smorken\String2Blade\ServiceProvider;
use Spatie\LaravelData\LaravelDataServiceProvider;

class TestBenchTestCase extends TestCase
{
    protected function defineEnvironment($app)
    {
        $app['config']->set('stringyform.layout', 'stringyform::master');
        CacheAssist::setCache($app[CacheManager::class]);
    }

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\Export\ServiceProvider::class,
            \Smorken\Roles\ServiceProvider::class,
            \Smorken\Auth\ServiceProvider::class,
            ServiceProvider::class,
            \Smorken\StringyForm\ServiceProvider::class,
            \Smorken\Data\ServiceProvider::class,
            ActionServiceProvider::class,
            RepositoryServiceProvider::class,
            ModelServiceProvider::class,
            PolicyServiceProvider::class,
            DomainServiceProvider::class,
            LaravelDataServiceProvider::class,
        ];
    }
}
