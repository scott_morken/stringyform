<?php

namespace Smorken\StringyForm\Services\Entries\Admin;

use Smorken\Service\Services\CrudByStorageProviderServices;

class CrudServices extends CrudByStorageProviderServices implements \Smorken\StringyForm\Contracts\Services\Entries\Admin\CrudServices {}
