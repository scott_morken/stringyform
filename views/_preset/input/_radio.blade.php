@php
    $name = $name ?? \Smorken\StringyForm\Tools\ElementHelper::name('input_radio');
    $value = $value ?? 1;
    $id = $id ?? \Smorken\StringyForm\Tools\ElementHelper::ensureId($name.'-'.$value);
    $attrs = [
        'id' => $id,
        'type' => 'radio',
        'class' => 'form-check-input '.($classes ?? ''),
        'name' => $name,
        'value' => $value,
        'checked' => (bool) ($checked ?? old($name, isset($entry) ? $entry->elementHasValue($name, $value) : false)),
    ];
@endphp
<input {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($attrs)) !!} {!! (new \Smorken\StringyForm\Tools\HtmlAttributes($add_attrs ?? [])) !!}/>
