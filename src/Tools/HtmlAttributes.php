<?php

namespace Smorken\StringyForm\Tools;

use Smorken\StringyForm\Contracts\Tools\HtmlAttribute;

class HtmlAttributes implements \Smorken\StringyForm\Contracts\Tools\HtmlAttributes
{
    protected array $appends = ['class'];

    protected array $attributes = [];

    public function __construct(array $attributes = [])
    {
        $this->addMany($attributes);
    }

    public function __toString(): string
    {
        return implode(' ', array_filter($this->all(), fn ($item) => $item->shouldRender()));
    }

    public function add(HtmlAttribute $attribute): static
    {
        $index = $this->indexOf($attribute->key);
        if (! is_null($index) && in_array($attribute->key, $this->appends)) {
            return $this->append($attribute);
        }
        if (! is_null($index)) {
            $this->attributes[$index] = $attribute;
        } else {
            $this->attributes[] = $attribute;
        }

        return $this;
    }

    public function addKeyValue(string $key, mixed $value): static
    {
        $htmlAttribute = new \Smorken\StringyForm\Tools\HtmlAttribute($key, $value);
        $this->add($htmlAttribute);

        return $this;
    }

    public function addMany(array $attributes): static
    {
        foreach ($attributes as $key => $value) {
            if ($value instanceof HtmlAttribute) {
                $this->add($value);
            } elseif (is_string($key)) {
                $this->addKeyValue($key, $value);
            }
        }

        return $this;
    }

    public function all(array $additional = []): array
    {
        $this->addMany($additional);

        return $this->attributes;
    }

    public function append(HtmlAttribute $attribute): static
    {
        $existing = $this->get($attribute->key);
        if ($existing) {
            $existing->value = implode(' ', array_filter([$existing->value, $attribute->value]));
        } else {
            $this->attributes[] = $attribute;
        }

        return $this;
    }

    public function get(string $key): ?HtmlAttribute
    {
        $index = $this->indexOf($key);

        return ! is_null($index) ? $this->attributes[$index] : null;
    }

    public function indexOf(string $key): ?int
    {
        foreach ($this->all() as $index => $attribute) {
            if ($attribute->key === $key) {
                return $index;
            }
        }

        return null;
    }
}
