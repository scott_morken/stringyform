<?php

namespace Tests\Smorken\StringyForm\Feature\Http\Controllers\Admin;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Route;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Smorken\Roles\ServiceProvider;
use Smorken\Roles\Support\DefineGates;
use Smorken\StringyForm\Http\Controllers\Entry\EntryController;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\CanStripNonChars;
use Tests\Smorken\StringyForm\Feature\Http\Controllers\Entry\Traits\HasForms;
use Tests\Smorken\StringyForm\TestBenchTestCase;

class StringyFormControllerTest extends TestBenchTestCase
{
    use CanStripNonChars, DatabaseMigrations, HasForms;

    public function testIndexNoRecords(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $response = $this->actingAs($user)
            ->get('/admin/form');
        $response->assertSee('No records found');
    }

    public function testIndexWithForms(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form1 = $this->getComplexForm();
        $form2 = $this->getSimpleForm();
        $response = $this->actingAs($user)
            ->get('/admin/form');
        $response->assertDontSee('No records found');
        $response->assertSee($form1->name);
        $response->assertSee($form2->name);
        $response->assertSee('/admin/form/view/'.$form1->id);
        $response->assertSee('/admin/form/view/'.$form2->id);
    }

    public function testNonAdminUserIsNotAuthorized(): void
    {
        $user = User::factory(['id' => '12345'])->create();
        $response = $this->actingAs($user)
            ->get('/admin/form');
        $response->assertStatus(403);
        $response->assertSee('This action is unauthorized');
    }

    public function testPostToCreate(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form = $this->getSimpleFormBladeString();
        $response = $this->actingAs($user)
            ->post('/admin/form/create', [
                'name' => 'Test Form',
                'form' => $form,
                'element_rules' => "test=>required\ncheck-test=>boolean",
            ]);
        $response->assertSessionHas('flash:success', 'Saved resource [1].');
        $response->assertRedirect('http://localhost/admin/form');
    }

    public function testPostToCreateWithValidationErrors(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form = $this->getSimpleFormBladeString();
        $response = $this->actingAs($user)
            ->post('/admin/form/create', [
                'name' => 'Test Form',
            ]);
        $response->assertSessionHasErrors(['form', 'element_rules']);
    }

    public function testPreviewWithForm(): void
    {
        Route::get('/entry', [EntryController::class, 'index']);
        $user = User::factory(['id' => '1'])->create();
        $form = $this->getComplexForm();
        $response = $this->actingAs($user)
            ->get('/admin/form/preview/'.$form->id);
        $response->assertSee('<button id="submit-form" type="submit" name="submit_form" class="btn" >', false);
    }

    public function testUpdateWithForm(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form = $this->getComplexForm();
        $response = $this->actingAs($user)
            ->get('/admin/form/update/'.$form->id);
        $response->assertSee($form->name);
        $response->assertSee('rows="15"><form method="post">'.PHP_EOL.'    @include(', false);
    }

    public function testViewWithForm(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $form = $this->getComplexForm();
        $response = $this->actingAs($user)
            ->get('/admin/form/view/'.$form->id);
        $response->assertSee($form->name);
        $response->assertSee('<pre>&lt;form method=&quot;post&quot;&gt;', false);
        $response->assertSee('<pre>&lt;div&gt;Test: {{ $entry-&gt;elementValue(&#039;test&#039;) }}&lt;/div&gt;</pre>',
            false);
        $response->assertSee('col-sm-12">required|array|in:blue,red,green</div>', false);
    }

    public function testViewWithNoForm(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $response = $this->actingAs($user)
            ->get('/admin/form/view/99');
        $response->assertStatus(404);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RoleSeeder::class);
        RoleUser::create(['user_id' => '1', 'role_id' => 1]);
        ServiceProvider::defineGates($this->app, true);
        DefineGates::reset(true);
    }
}
