<?php

namespace Smorken\StringyForm\Contracts\Services\Entries;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\StringyForm\Contracts\Models\Entry;

interface RetrieveService extends \Smorken\Service\Contracts\Services\RetrieveService
{
    public function findByFormId(Request $request, int $formId): ModelResult;

    public function getModel(): Entry;
}
