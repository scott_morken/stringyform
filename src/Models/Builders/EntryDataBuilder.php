<?php

declare(strict_types=1);

namespace Smorken\StringyForm\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;

class EntryDataBuilder extends Builder
{
    public function entryIdIs(int $entryId): EloquentBuilder
    {
        /** @var $this */
        return $this->where('entry_id', '=', $entryId);
    }
}
